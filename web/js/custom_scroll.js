
var customScroll = {
	createScroll: function(el, options) {
		var el = el;
		[].forEach.call(el, function(el) {
			var 
				currentHeight = el.clientHeight,
				fullScrollHeight = Math.max( el.scrollHeight, el.offsetHeight, el.clientHeight ),
				scrollPos = el.scrollTop,
				scrollStep = 0,
				cantouch = ("ontouchstart" in document.documentElement) || ("ontouchstart" in window),
				downEvent = cantouch ? "touchstart" : "mousedown",
				moveEvent = cantouch ? "touchmove" : "mousemove",
				upEvent = cantouch ? "touchend" : "mouseup";
				
			if ( fullScrollHeight <= currentHeight ) return;

			var settings = {
				arrows: false
			}

			for (var key in settings) {
				if (options.hasOwnProperty(key)) {
					settings[key] = options[key];
				}
			}

			el.style.overflow = 'hidden';

			var scroll = document.createElement('div');
			scroll.className = "custom_scroll";
			var knob = document.createElement('div');
			knob.className = "knob";
			scroll.appendChild(knob);
			el.parentNode.appendChild(scroll);
			var topArrow = settings.arrows ? document.createElement('span') : null;
			if (topArrow != null)  {topArrow.className = "top_arrow"; scroll.appendChild(topArrow);}
			var bottomArrow = settings.arrows ? document.createElement('span') : null;
			if (bottomArrow != null)  {bottomArrow.className = "bottom_arrow"; scroll.appendChild(bottomArrow);}

			scrollStep = (fullScrollHeight - currentHeight) / (scroll.clientHeight - knob.clientHeight);

			function scrollIt() {
				if ( scrollPos < 0 ) {
					scrollPos = 0;
				} else if ( scrollPos > (scroll.clientHeight - knob.clientHeight) ) {
					scrollPos = (scroll.clientHeight - knob.clientHeight);
				}

				knob.style.top = scrollPos + "px";

				el.scrollTop = scrollStep* scrollPos;
			}


			//mousewheel
			el.addEventListener('DOMMouseScroll', scrollContent, false);
			el.addEventListener('mousewheel', scrollContent, false);
			
			function scrollContent(e){
				e.preventDefault(e);

				if(e.type == 'DOMMouseScroll'){
					if(e.detail > 0){
						scrollPos += 20;
					}else{
						scrollPos -= 20;
					}
				} else{
					if(e.wheelDelta < 0){
						scrollPos += 20;
					}else{
						scrollPos -= 20;
					}
				}

				knob.style.top = scrollPos + 'px';
				scrollIt();
			}

			//touchscroll
			if (cantouch) {
				el.style.overflow = "auto";
				el.addEventListener('scroll', function() {
					knob.style.top = ((scroll.clientHeight - knob.clientHeight) / (fullScrollHeight - currentHeight))* this.scrollTop + 'px';
				}, false)
			}

			//arrows
			if (settings.arrows == true) {
				function arrowClick() {
					if (this.className == 'top_arrow') {
						scrollPos -= 20;
					} else {
						scrollPos += 20;
					}

					knob.style.top = scrollPos + 'px';
					scrollIt();
				}

				topArrow.addEventListener('click', arrowClick, false);
				bottomArrow.addEventListener('click', arrowClick, false);
			}

			//drag knob
			var offsetYKnob;
			var doc = document.documentElement;

			knob.addEventListener(downEvent , startScroll, false);
			document.querySelectorAll('html')[0].addEventListener(upEvent, stopScroll, false);
			document.body.addEventListener('mouseleave', stopScroll, false);
			
			function startScroll(e){
				var wTop = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);

				offsetYKnob = cantouch ? e.changedTouches[0].clientY - scrollPos + wTop : e.clientY - scrollPos + wTop;
				document.querySelectorAll('html')[0].addEventListener(moveEvent, moveKnob, false);
				e.preventDefault();
			}
			
			function moveKnob(e){
				var knobNewPos = cantouch ? e.changedTouches[0].clientY - offsetYKnob : e.clientY - offsetYKnob;
				var wTop = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
				scrollPos = (knobNewPos + wTop);
				
				e.preventDefault();
				scrollIt();

			}
			
			function stopScroll() {
				document.querySelectorAll('html')[0].removeEventListener(moveEvent, moveKnob, false);
			}
		}, false);

		//responsive
		var resizeTimeout;
		function resizeWindow() {
			window.removeEventListener('resize', resizeWindow, false);
			clearTimeout(resizeTimeout);
			resizeTimeout = setTimeout(function() {
				customScroll.destroyScroll(el, options);
			}, 300);
		}
		
		window.addEventListener('resize', resizeWindow, false);
	},
	destroyScroll: function(scroll, options) {
		var c_scroll = scroll[0].parentNode.querySelectorAll('.custom_scroll')[0];
		c_scroll.parentNode.removeChild(c_scroll);
		customScroll.createScroll(scroll, options);
	}
}