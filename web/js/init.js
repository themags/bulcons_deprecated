// JavaScript Document
document.documentElement.className = document.documentElement.className.replace(/\bno-js\b/g, '') + 'js';
$(function() {
    /* The only thing we do, when the DOM is loaded, is start the init function of our wrapped in namespace app */
    mainApp.init();
});

/* All the UI required functions have their own scope */
var mainApp = (function() {
    /* All constants and other variables should be defined in the CONFIG object */
    var config = {
        /* MY_CONSTANT_NAME: value */
        cantouch: ("ontouchstart" in document.documentElement) || ("ontouchstart" in window),
        wmmq : window.matchMedia("screen and (max-width: 768px)")
    }

    /* Initialisation of all the named functions, that the UI requires */
    function init() {
        /* myUIFunction(); */
        content_height();
        open_lang();
        home_carousel();
        carousel_icons();
        list_partners_carousel();
        menu();
        side_sub_nav();
        side_menu_opener();
        img_switch();
        open_careers_form();
        tabs();
        table_acc();
        // 

        popup_gallery();
        product_gallery();
        accordion();
        popup_message();
    
        $('div.accordion .opened + .data').hide().show();

        $('select.custom').transform_selectboxes();
        $('input:radio,input:checkbox').transform_inputs();
        $('.input-file input[type=file]').change(function(){
            $(this).next().find('input').val($(this).val());
        });

        $('.datepicker').datepicker();

        $('[data-label-prevent="true"]').each(function() {
            $(this).click(function(e) {
                e.preventDefault();
                return false;
            });
        });

        $(window).bind('debouncedresize', content_height);
    }

    function content_height() {
        setTimeout(function() {
            var content_h = $(window).height() - ($('header').outerHeight() + $('footer').outerHeight());
            $('#page').css({
                'min-height': content_h+'px'
            });
        }, 0);
    }

    function accordion() {
        $(document.body).delegate('.accordion .title:not(.permanent)', 'click', function(e) {
            var 
                $this = $(this),
                is_single = $this.closest('.accordion').hasClass('single');
            
            if ($(e.target).is(':not(:checkbox)')) {
                if (is_single) {
                    $this.next().slideToggle('fast', function() {
                        var title = $(this).prev();
                        if ($(this).is(':visible')) {
                            if (title.data('after-open-callback')) {
                                window[title.data('after-open-callback')].call(title);
                            }
                        }
                        
                        title.toggleClass('opened');
                    });
                } else {
                    $this.siblings('.data:not(:eq(' + $this.siblings('.title').addBack().index(this) + ')):visible').slideToggle('fast', function() {
                        $(this).prev().toggleClass('opened');
                    }).end().next().slideToggle('fast', function() {
                        var title = $(this).prev();
                        if ($(this).is(':visible')) {
                            if (title.data('after-open-callback')) {
                                window[title.data('after-open-callback')].call(title);
                            }
                        }
                        
                        title.toggleClass('opened');
                    });
                }
            }
        });
    }

    function menu() {
        $('#menu-mobile').click(function () {
            $(this).toggleClass('is-active');
            $('.header-nav').toggleClass('active');
        });

        if (!config.cantouch) {
            $('.has-sub').off('mouseover mouseleave').on('click', function() {
                $(this).toggleClass('active');
            });
        } else {
            $('.has-sub').off('click').hover(
                function() {
                    $(this).addClass('active');
                },
                function() {
                    $(this).removeClass('active');
                }
            );
        }
    }

    function popup_gallery() {
        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            closeOnContentClick: false,
            image: {
                verticalFit: true
            },
            gallery: {
                enabled: true,
                preload: [0,1]
            },
            callbacks: {
                buildControls: function() {
                    this.items.length > 1 && this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                }
            }
        });
    }

    function product_gallery() {
        var 
            thumbs_content;
        
        function product_gallery_listener(mql) {
            var owl = $('.thumbs');

                owl.clone(true)

            if (!thumbs_content) {
                thumbs_content = owl.clone(true);
            }

            if (!!owl.data('owlCarousel')) {
                var cloned = thumbs_content.clone(true);
                owl.replaceWith(cloned);   
                owl = cloned;
            }

            if (mql.matches) {
                owl.owlCarousel({
                    items: 1,
                    nav: true,
                    loop: true,
                    margin: 10
                });
            } else {
                owl.find('> a:even').each(function(idx) {
                    $([this, $(this).next().get(0)]).wrapAll('<div />')
                }).end().owlCarousel({
                    items: 2,
                    nav: true,
                    margin: 10
                });
            }

            $('.thumbs').each(function() {
                $(this).find('a').click(function(e) {
                    e.preventDefault();
                    $(this).closest('aside').find('> a img').attr('src', $(this).attr('href'));
                    return false;
                });
            });
        }

        product_gallery_listener(config.wmmq);
        config.wmmq.addListener(product_gallery_listener);
    }

    function open_careers_form() {
        $('.form-open').click(function () {
            $(this).toggleClass('active');
            $('.careers-form').toggle();
        });
    }

    function open_lang() {
        $('.language').click(function () {
            $(this).toggleClass('language-open');
        });
    }

    function side_sub_nav() {
        $('.side-has-sub').click(function () {
            $(this).toggleClass('active');
        });
    }

    function side_menu_opener() {
        $('.side-menu-opener').click(function () {
            $(this).toggleClass('active');
            $(this).next().toggleClass('opened');
        });
    }

    function home_carousel() {
        $('.carousel-home').owlCarousel({
            items: 1,
            autoHeigth: true
        });
    }

    function carousel_icons() {
        $('.carousel-icons').owlCarousel({
            nav: true,
            margin: 10,
            responsive: {
                0: {
                    items: 1
                },
                699: {
                    items: 3
                },
                1024: {
                    items: 4
                },
                1920: {
                    items: 6
                }
            }
        });
    }

    function list_partners_carousel() {
        $('.list-partners-carousel').owlCarousel({
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 2
                },
                699: {
                    items: 3
                }
            }
        });
    }

    function img_switch() {
        enquire.register("screen and (max-width: 1024px)", {
            match : function() {
                $('.responsive-bgr').each(function() {
                    var imgURL = $(this).attr('src');
                    $(this).parent('div').css('background-image', 'url(' + imgURL + ')');
                });

                // $('.responsive-bgr').each(function() {
                //     var 
                //         $this = $(this),
                //         current_bgr = $this.css('background-image').replace(/^url\(|\)$/g, "");
                //         swap_bgr = $this.data('desktop-bgr');
                //     $this.css('background-image', 'url(' + swap_bgr + ')').data('desktop-bgr', current_bgr);
                // });
            },
            unmatch : function() {
                $('.responsive-bgr').each(function() {
                    var imgURL = $(this).attr('src');
                    $(this).parent('div').css('background-image', '');
                });
            }
        });
    }

    function tabs() {
        $('.tabs li').click(function(e) {
            $(this).addClass('current').siblings('li').removeClass('current').end().parent().siblings('.data').hide().eq($(this).index()).show().removeData('loaded');
        }).filter('.current').trigger('click');
    }

    function table_acc() {
        $('.departments-list-wrapper tr td:first-child').click(function(e) {
            $(this).parent().toggleClass('opened').end().parent().siblings().removeClass('opened');
        });
    }

    function popup_message() {
        $('.open-popup-link').magnificPopup({
            type:'inline',
            midClick: true
        });
    }

    /* Definitions for the named functions, required for the UI go here */
    /* function myUIFunction(param1, param2, ...) {
        //Function body
    } */

    /* Exposed methods are defined in the returned object */
    return {
        init: init
    };

}());
    
$.fn.transform_selectboxes = function() {
    return $(this).each(function() {
        var 
            $select = $(this);
        
        var $span = $('<span class="custom-selectbox" />').html(this.options[this.selectedIndex].text).insertBefore(this);
            $select.outerWidth($span.outerWidth());
            $(window).resize(function(){
                $select.outerWidth($span.outerWidth());
            });
        if ($select.data('selectbox-width') === 'exact') {
            var nw = $select.outerWidth() + 10; 
            $select.outerWidth(nw);
            $span.outerWidth(nw);
        }
        
        $select.css('opacity', 0).change(function() {
            $span.html(this.options[this.selectedIndex].text);
        });
    });
};
    
$.fn.transform_inputs = function() {
    return $(this).each(function() {
        if (!$(this).data('transformed')) {
            $(this).data('transformed', true);
            var 
                inp = this, 
                is_radio = this.type === 'radio';
            
            var $span = $('<span class="' + (this.disabled ? 'disabled ' : '') + this.type + (this.checked ? ' checked' : '')+'" />').insertBefore(this);
            $(inp).change(function() {
                if (is_radio) {
                    $('input:radio[name="' + inp.name + '"]').not(this).prop('checked', false).prev().removeClass('checked');
                }
                
                $span[inp.checked ? 'addClass' : 'removeClass']('checked');
            });
        }
    });
};

/*
 * debouncedresize: special jQuery event that happens once after a window resize
 * $(window).bind('debouncedresize', resize);
 */
(function($) {

    var $event = $.event,
        $special,
        resizeTimeout;

    $special = $event.special.debouncedresize = {
        setup: function() {
            $( this ).on( "resize", $special.handler );
        },
        teardown: function() {
            $( this ).off( "resize", $special.handler );
        },
        handler: function( event, execAsap ) {
            // Save the context
            var context = this,
                args = arguments,
                dispatch = function() {
                    // set correct event type
                    event.type = "debouncedresize";
                    $event.dispatch.apply( context, args );
                };

            if ( resizeTimeout ) {
                clearTimeout( resizeTimeout );
            }

            execAsap ?
                dispatch() :
                resizeTimeout = setTimeout( dispatch, $special.threshold );
        },
        threshold: 150
    };

})(jQuery);