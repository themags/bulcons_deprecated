(function($) {

	$('.menu-trigger').click(function() {
		$(this).toggleClass('active').parent().find('nav').slideToggle();
		$('.languages-list > ul').slideUp();
		$('.languages-list > a').removeClass('active');
	});

	$('nav li.has-child').click(function(){
		$(this).toggleClass('active').find('ul').slideToggle();
		$('.profile > a').removeClass('active');
		$('.profile .login').slideUp();	
		$('.languages-list > a').removeClass('active');
		$('.languages-list ul').slideUp();
		$(this).siblings('.has-child.active').find('ul').slideToggle().end().removeClass('active');
	});

	$('.languages-list > a').click(function() {
		$(this).toggleClass('active').parent().find('ul').slideToggle();
		$('.profile > a').removeClass('active');
		$('.profile .login').slideUp();
		$('nav .has-child.active').find('ul').slideUp().end().removeClass('active');
	});

	$('.languages-list ul li.active img').clone().appendTo($('.languages-list > a'));
	$('.languages-list ul li').click(function() {
		var src = $(this).find('img').attr('src');
		$('.languages-list > a').removeClass('active').find('img').attr('src', src);
		$(this).parent().slideUp();
	});

	$('.profile > a').click(function() {
		$(this).toggleClass('active').next().slideToggle();
		$('.languages-list > a').removeClass('active');
		$('.languages-list ul').slideUp();
		$('nav .has-child.active').find('ul').slideUp().end().removeClass('active');	
	});

	if($('.main-carousel .item').length > 1) {
		$('.main-carousel').owlCarousel({
			items: 1,
			responsive: {
				768: {
					nav: true,
					navText: ['&#xe901;','&#xe902;']
				}
			}
		});		
	}

	$('.product-carousel').owlCarousel({
		items: 1,
		dots: true,
		autoplay: true,
		autoplaySpeed: 500,
		loop: true
	});

	$('.highlights .carousel').owlCarousel({
		responsive : {
			320 : {
				items: 1,
				nav: true,
				navText: ['&#xe901;','&#xe902;']
			},
			768: {
				items: 2,
				nav: true,
				navText: ['&#xe901;','&#xe902;']
			},
			1024 : {
				items: 3,
				nav: true,
				navText: ['&#xe901;','&#xe902;']
			},
			1366 : {
				items: 4,
				nav: false
			}
		},
	});

	$('.used-products .carousel').owlCarousel({
		items: 1,
		nav: ($('.used-products .carousel .item').length > 1) ? true : false,
		navText: ['&#xe901;','&#xe902;']
	});

	$('.half .carousel').owlCarousel({
		items: 1,
		nav: ($('.half .carousel li').length > 1) ? true : false,
		navText: ['&#xe901;','&#xe902;'],
		responsive : {
			768: {
				items: 2,
				nav: ($('.half .carousel li').length > 2) ? true : false
			}
		}
	});


	function product_page_carousel() {
		var el = $('.wide .carousel li').length;

		if(el > 1) {
			$('.wide .carousel').owlCarousel({
				items: 1,
				nav: true,
				navText: ['&#xe901;','&#xe902;'],
				responsive : {
					768: {
						items: 2,
						nav: (el > 2) ? true : false
					},
					1024: {
						items: 3,
						nav: (el > 3) ? true : false
					},
					1366: {
						items: 4,
						nav: (el > 4) ? true : false
					}
				}
			});
		}
	}
	product_page_carousel();

	$('.production-carousel').owlCarousel({
		responsive : {
			320 : {
				items: 1,
				nav: true,
				navText: ['&#xe901;','&#xe902;']
			},
			768: {
				items: 3,
				nav: true,
				navText: ['&#xe901;','&#xe902;']
			},
			1024 : {
				items: 4,
				nav: true,
				navText: ['&#xe901;','&#xe902;']
			}
		},
	});

	$('.tab-list p').click(function() {
		if ($(this).next('ul').is(":visible")) {
			$(this).next('ul').slideUp(function(){
				$(this).parent().removeClass('opened');
			});
		} else {
			$(this).parent().addClass('opened');
			$(this).next('ul').slideDown();
		}
	});

	if ($(window).width() < 1024){
		$('.tab-list li').click(function() {
			var current = $(this);
			current.closest('.tab-list').find('p').text($(this).text());
			current.closest('.tab-list').find('ul').slideUp(function() {
				$(this).parent().removeClass('opened');			
			});
			$.ajax({
				url: current.data('url'),
				method: 'post',
				dataType: 'json'
			}).done(function(json) {
				current.closest('.tab-wrapper').find('.recipies-list').html(json.html);
			});
		});
	} else {
		$('.tab-list li').click(function() {
			var current = $(this);
			current.addClass('current').siblings().removeClass('current');
			$.ajax({
				url: current.data('url'),
				method: 'post',
				dataType: 'json'
			}).done(function(json) {
				current.closest('.tab-wrapper').find('.recipies-list').html(json.html);
			});
		});
	}

	$('.gallery li').magnificPopup({
		delegate: 'a',
		type: 'image',
		mainClass: 'steps-gallery',
		gallery:{
			enabled:true
		}
	});

	$('.certificates-list li').magnificPopup({
		delegate: 'a',
		type: 'image'
	});

	$('.production-carousel a, .product-wrap .item a').magnificPopup({
		type: 'image'
	});

	$('.apply').click(function() {
		$(this).addClass('active');
		$(this).next('.apply.form').slideDown();
	});
	
	$('.apply.form input[type="file"]').change(function(){
		var filename = $(this).val();
		$(this).parent().find('span').text(filename);
	});

	$('.accordeon h4').click(function() {
		$(this).toggleClass('active').next('p').slideToggle();
		$(this).parent().siblings().find('h4').removeClass('active');
		$(this).parent().siblings().find('p').slideUp();
	});

	$('.wrap-info .details-wrapper .product-details').not('.current').addClass('current').owlCarousel({
		onInitialized: function() {
			$(this.$element).removeClass('current');
		},
		responsive: {
			320: {
				items: 2,
				nav: true,
				navText: ['&#xe901;','&#xe902;'],
			},
			768: {
				items: 4,
				nav: true,
				navText: ['&#xe901;','&#xe902;'],
			},
			1024: {
				items: 7,
				nav: false
			}
		}
	});

	$('.product-wrap .product').owlCarousel({
		items: 1,
		nav: true,
		navText: ['&#xe901;','&#xe902;'],
		onTranslated: function() {
			var current = $('.wrap-info').find('.owl-item.active').index();
			$('.wrap-info').find('.product-details').eq(current).addClass('current').siblings().removeClass('current');
		}
	});
	
	$('.wrap-info').find('.product-details').trigger('refresh.owl.carousel');

	$('.product-details.current').owlCarousel({
		responsive: {
			320: {
				items: 2,
				nav: true,
				navText: ['&#xe901;','&#xe902;'],
			},
			768: {
				items: 4,
				nav: true,
				navText: ['&#xe901;','&#xe902;'],
			},
			1024: {
				items: 7,
				nav: false
			}
		}
	});

	$.fn.scroll_effect = function() {
		$(this).each(function() {
			var $this = $(this),
				element_offset = ($this.data('offset') === undefined) ? 0 : $this.data('offset'),
				timeout;

			function is_in_view(e) {
				var win = $(this),
					top = win.scrollTop();

				if (($this.offset().top + element_offset) < (win.height() + top)) {
					$this.addClass('in-view');
				}

				if ($this.hasClass('in-view') && top < 100) {
					$this.removeClass('in-view');
					setTimeout(function() {
						$this.addClass('in-view');
					}, 100);
				}
			}

			$(window).scroll(function(e) {
				clearTimeout(timeout);
				timeout = setTimeout(function() {
					is_in_view(e);
				}, 100);
			}).trigger('scroll');
		});
	}

	
	rating();
	$(document).ready(function() {
		var cantouch = ("ontouchstart" in document.documentElement) || ("ontouchstart" in window);


		if (cantouch) {
			$('body').addClass('touch');
		} else {
			$('body').addClass('no-touch');
		}

		if (!cantouch) {
			// $('[data-scroll="true"]').scroll_effect();

			$('.product-details').each(function() {
				var num = 0;
				$(this).find('.item').each(function() {
					$(this).css({
						'-webkit-transition-delay': num + 's',
						'transition-delay': num + 's'
					});
					num += 0.1;
				});
			});
		}
	});

})(jQuery);
function rating() {
	var stars = $('.rating li'),
		active_stars = $('.rating:not(.static) li'),
		
		rate_level = stars.parent().attr('data-rate');
		stars.parent().css({
			'width':  rate_level + '%'
		});
	
	active_stars.mouseenter(function() {
		var rate = ($(this).index()+1)*20;
		if (!$(this).parent().hasClass('rated')) {
			$(this).parent().css({
				'width':  rate + '%'
			});
		}
	});
	
	active_stars.mouseleave(function() {
		var rate_level = $(this).parent().data('rate');
		if (!$(this).parent().hasClass('rated')) {
			$(this).parent().css({
				'width':  rate_level + '%'
			});
		}
	});

	active_stars.click(function() {
		var rate_val = $(this).index()+1,
			ul = $(this).parent(),
			rate = ($(this).index()+1)*20;

		if (!ul.hasClass('rated')) {
			ul.addClass('rated');
			ul.attr('data-rate', rate);
			ul.css({
				'width':  rate + '%'
			});
		};

		if(rate_val == 5){
			ul.addClass('exellent');
		} else {
			ul.removeClass('exellent');
		}
	});
}