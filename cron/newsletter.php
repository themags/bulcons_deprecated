<?php
	require(dirname(dirname(__FILE__)) . '/core/sweboo.php');
	require_once(Config()->LIB_PATH . 'smarty/SmartyBC.class.php');
	$template = new SmartyBC();
	Registry()->tpl = $template;
	$action = new ActionController();
	$user_model = new UsersModel();
	$users = $user_model->find_all('truck_subscription is NOT NULL OR part_subscription is NOT NULL');
	$thumb = new ThumbnailHelper();
	$smarty = new Smarty_Internal_Template();

	foreach($users as $user) {
		$subscription_data = array();
		array_push($subscription_data, $user->truck_subscription);
		array_push($subscription_data, $user->part_subscription);

		foreach($subscription_data as $s) {
			if(!empty($s)) {
				$query = unserialize($s);
				Registry()->locale = $query['lang'];
				$_labels = Yaml::loadFile('../locales/' . $query['lang'] . '/_labels.yaml');
				$page = new Page();
				$page = $page->find((($query['type'] == 1) ? Config()->TRUCK_FILTER_PAGE_ID : Config()->PARTS_FILTER_PAGE_ID));
				$model = (($query['type'] == 1) ? new TrucksModel() : new PartsModel());

				$where = "";
				if(!empty($query['search_word']) && $query['search_word'] != "")
				$where .= " AND content LIKE '%" . $query['search_word'] . "%' OR summary LIKE '%" . $query['search_word'] . "%'" ;

				if(!empty($query['modification']) && $query['modification'] != "")
					$where .= " AND modification LIKE '%" . $query['modification'] . "%'";

				if(is_array($query['category_id']))
					$where .= " AND " . (($query['type'] == 1) ? 'truck_category_id' : 'parts_category_id') . " IN (" . implode(",", $query['category_id']) . ")";

				if(!empty($query['brand_id']) && (int)$query['brand_id'])
					$where .= " AND " . (($query['type'] == 1) ? 'truck' : 'parts') . '_brands_id = ' . $query['brand_id'];

				if(!empty($query['model']))
					$where .= " AND model = '" . $query['model'] . "'";

				if(!empty($query['kilometers']) && (int)$query['kilometers'])
					$where .= " AND kilometers <= '" . $query['kilometers']."'";

				if(!empty($query['price']) && (int)$query['price'])
					$where .= " AND price <= '" . $query['price'] ."'";

				if(!empty($query['year']) && (int)$query['year'])
					$where .= " AND year = '" . $query['year'] ."'";

				$order = 'created_at DESC';
				if(!empty($query['order_by'])) {
					$orders = array('date' => 'created_at DESC', 'price_asc' => 'price ASC', 'price_desc' => 'price DESC');
					$order = $orders[$query['order_by']];
				}

				$products = $model->find_all('active=1' . $where, $order);
				$products_ids = array();

				foreach($products as $p) {
					array_push($products_ids, $p->id);
				}

				$subscription_model = new Subscription();
				$result_subscription = $subscription_model->find_all('user_id = ' . $user->id . ' AND type = ' . $query['type']);

				$products_subscriptions_ids = array();
				foreach($result_subscription as $p) {
					array_push($products_subscriptions_ids, $p->product_id);
				}

				$diff = array_diff($products_ids, $products_subscriptions_ids);

				if(!empty($diff)) {
					$diff = array_slice($diff, 0, 6); //limits
					$partial_html = '';
					foreach($diff as $id) {
						$html = file_get_contents('partial.htm');

						$image = "";
						if(!empty($products[$id]->picture->filename)) {
							$image = $thumb->thumb_smarty(array('file' => $products[$id]->picture->get_file_url(), 'width' => 277, 'height' => 213, 'method' => 'fit', 'full_url' => true), $smarty);
						}

						$partial_html .= str_replace(
							array(
								'{BRAND}',
								'{IMAGE}',
								'{MODEL}',
								'{LOCATION}',
								'{PRICE}',
								'{DATE}',
								'{MODIFICATION}',
								'{SUMMARY}',
								'{SEE_MORE}',
								'{URL}',
								'{DOMAIN}'
							), array(
								strip_tags((($query['type'] == 1) ? $products[$id]->truck_brands : $products[$id]->parts_brands)),
								$image,
								strip_tags($products[$id]->model),
								strip_tags($products[$id]->location),
								strip_tags($products[$id]->price),
								date('d.m.Y H:i:s', strtotime($products[$id]->created_at)),
								strip_tags($products[$id]->modification),
								mb_substr(strip_tags($products[$id]->summary), 0, 400) . ((mb_strlen(strip_tags($products[$id]->summary)) > 400) ? '...' : ''),
								$_labels['BUTTONS']['see_more'],
								Config()->DOMAIN . strtolower(substr(Registry()->locale, 0, 2)) . $page->slug . '/' . $products[$id]->slug,
								Config()->DOMAIN
							), $html
						);
					}

					$html = file_get_contents('html.htm');
					$html = str_replace(
						array('{TITLE}', '{HTML}', '{UNSUBSCRIBE_TEXT}', '{UNSUBSCRIBE}'),
						array($_labels['SUBSCRIPTION']['title'][$query['type']], $partial_html, $_labels['SUBSCRIPTION']['unsubscribe'], Config()->DOMAIN . strtolower(substr(Registry()->locale, 0, 2)) . '/profiles/unsubscribe?key=' . $user->unsubscribe_code . '&type=' . $query['type']),
						$html
					);

					//send mail
					$plain = ''; //to do
					if(!send_php_mail(array(
						'from'		=> Config()->EMAILS_FROM,
						'mail'		=> $user->email,
						'subject'	=> $_labels['SUBSCRIPTION']['title'][$query['type']],
						'plain'	=> $plain,
						'html'		=> $html
					))) {
						//save emails log
						$log = 'Failed to send - IP: ' . $_SERVER["REMOTE_ADDR"] . '; EMAIL: ' . $user->email . ';' . PHP_EOL;
						file_put_contents(Config()->ROOT_PATH . 'logs/' . date('Y_m_d') . '.log', $log, FILE_APPEND);
					} else {
						//save in db
						foreach($diff as $id) {
							Registry()->db->query('INSERT INTO subscriptions VALUES(' . $user->id . ', ' . $products[$id]->id . ', ' . $query['type'] . ')');
						}
					}
				}
			}
			//print_r($html);
		}
	}
?>