<?php

class User extends Modules {
	public
		$table_name = 'users',
		$orig_table_name = 'users',
		$preserve_index = true;

	protected
		$is_i18n = false,
		$has_mirror = false;

	function before_validation() {
		if(!empty($this->email)) {
			if(!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
				$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'invalid_email')), 'email');
			}

			if($this->find('email="' . $this->email . '"')) {
				$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'already_exists')), 'email');
			}
		}

		if(empty($this->confirm_password)) {
			$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'not_empty')), 'confirm_password');
		}

		if(!empty($this->password) && !empty($this->confirm_password)) {
			$this->check_passwords();
		}

		$this->_checkPhone();
	}

	function check_login() {
		if(empty($_POST['email'])) {
			$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'not_empty')), 'email');
		}

		if(empty($_POST['password'])) {
			$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'not_empty')), 'password');
		}
	}

	function check_and_update_user_details($post) {
		$required_fields = array('country', 'city', 'address', 'phone', 'mobile_phone', 'name', 'company');

		if(!empty(Registry()->session->user_data['email']) && !empty(Registry()->session->user_data['id'])) {
			if(!empty($post['password']) || !empty($post['new_password'])) {
				if(!$this->find(array('conditions' => 'email = "' . Registry()->session->user_data['email'] . '" AND password = sha1("' . $post['password'] . '")'))) {
					$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'wrong_password')), 'password');
				}

				if(empty($post['confirm_password'])) {
					$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'not_empty')), 'confirm_password');
				}

				if(empty($post['new_password'])) {
					$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'not_empty')), 'new_password');
				} else if (!empty($post['password'])) {
					$this->password = $post['new_password'];
					$this->confirm_password = $post['confirm_password'];
					$this->check_passwords(true);
				}
			}

			unset($post['password'], $post['new_password'], $post['confirm_password']);

			//if someone cheats with email
			if(!empty($post['email'])) {
				unset($post['email']);
			}

			$this->phone = $post['phone'];
			$this->mobile_phone = $post['mobile_phone'];
			$this->_checkPhone();

			foreach($required_fields as $field) {
				if(empty($post[$field])) {
					$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'not_empty')), $field);
				}
			}

			$data = "";
			$count = 0;
			$count_post = count($post);
			foreach($post as $key => $value) {
				$count++;
				$data .= $key . ' = "' . $value .  (($count_post == $count) ? '"' : '", ');
			}

			if(!empty($this->password) && $this->password != "") {
				$data .= ', password = "' . $this->password . '"';
			}

			if(!$this->update_all($data, 'id=' . Registry()->session->user_data['id'])) {
				$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'error')), 'other');
			}
		} else {
			$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'error')), 'other');
		}
	}

	function check_passwords($users_details = false) {
		$key = $users_details ? 'new_password' : 'password';
		if (preg_match("/^[\w\d\s.,-]*$/", $this->password)) { //checks if password contains only latin symbols
			if(strlen($this->password) >= 8 && strlen($this->password) <= 15) {
				if($this->password != $this->confirm_password) {
					$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'password_not_match')), $key);
				} else {
					$this->password = sha1($this->password);
				}
			} else {
				$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'password_wrong_length')), $key);
			}
		} else {
			$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'password_not_latin')), $key);
		}
	}

	function check_email_exists($email) {
		if(!empty($email)) {
			if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'invalid_email')), 'email');
			} else if(!$user = $this->find('email="' . $email . '"')) {
				$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'email_not_found')), 'email');
			}
		} else {
			$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'not_empty')), 'email');
		}

		if(empty($this->get_errors())) {
			$from = $to = $subject = $plain = $html = null;
			extract(Registry()->localizer->get_label('MAIL_FORGOTTEN_PASSWORD'), EXTR_OVERWRITE);
			$code = $this->generate_code();
			$this->update('id=' . $user->id, array('forgotten_password_code' => $code), 1);

			$replacements	= array('{URL_ACTIVATION}' => url_for(array("controller" => "profiles", "action" => "save_forgotten_password", "key" => $code)));
			$plain		= strtr($plain, $replacements);
			$html			= strtr($html, $replacements);
			$from		= Config()->EMAILS_FROM;
			$to			= $email;

			if(!send_php_mail(array(
				'from'		=> $from,
				'mail'		=> $to,
				'subject'	=> Registry()->localizer->get_label('MAIL_FORGOTTEN_PASSWORD', 'subject'),
				'plain'	=> $plain,
				'html'		=> $html
			))) {
				$this->add_save_error('not_send', 'other');
			}
		}
	}

	public function generate_code() {
		return substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 16);
	}

	private function _checkPhone() {
		if (!preg_match('/^[0-9+\-()\d]+$/i', $this->phone)) {
			$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'phone_not_correct')), 'phone');
		}

		if (!preg_match('/^[0-9+\-()\d]+$/i', $this->mobile_phone)) {
			$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'phone_not_correct')), 'mobile_phone');
		}
	}
}

?>