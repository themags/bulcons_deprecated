<?php 
class Search extends ActiveRecord {
	static function get_results($q) {
		$word = $q;
		if (strlen($word) < 4) {
			return false;
		}
		
		$word = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $word);
		$global_count = 0;
		
		$pages_model = new Page();
		$news_model = new NewsModel();
		$contacts_model = new ContactsModel();
		$managments_model = new ManagementsModel();
		$services_model = new ServicesModel();
		$locale = $services_model->get_locale();
		
		
		$result = array();
		

		$news_items = $news_model->find_all('active = 1 and i18n_locale = "'.$locale.'" and
				MATCH (title, content, summary) AGAINST ("'.$word.'" IN NATURAL LANGUAGE MODE)');
		if(!empty($news_items)) {
			$global_count += count($news_items);
			$news_items_page = $pages_model->find(121);	//121 is the  page
			$result += array('news' => $news_items, 'news_page' => $news_items_page);
		}
		

		$contacts_items = $contacts_model->find_all('active = 1 and 
				MATCH (title, summary, address1, address2, email) AGAINST ("'.$word.'" IN NATURAL LANGUAGE MODE)');
		if(!empty($contacts_items)) {
			$global_count += count($contacts_items);
			$contacts_items_page = $pages_model->find(122);	//122 is the  page
			$result += array('contacts' => $contacts_items, 'contacts_page' => $contacts_items_page);
		}
		
		$managments_items = $managments_model->find_all('active = 1 and i18n_locale = "'.$locale.'" and
				MATCH (title, content, summary) AGAINST ("'.$word.'" IN NATURAL LANGUAGE MODE)');
		if(!empty($managments_items)) {
			$global_count += count($managments_items);
			$managments_items_page = $pages_model->find(126);	//126 is the  page
			$result += array('managments' => $managments_items, 'managments_page' => $managments_items_page);
		}
		
		$services_items = $services_model->find_all('active = 1 and i18n_locale = "'.$locale.'" and
				MATCH (title, content, summary) AGAINST ("'.$word.'" IN NATURAL LANGUAGE MODE)');
		if(!empty($services_items)) {
			$global_count += count($services_items);
			foreach ($services_items as &$item) {
				$item->sp = $pages_model->find($item->page_id);
			}
		$result += array('services' => $services_items);
		}
		
		$pages_items = $pages_model->find_all('visibility = 2 and i18n_locale = "'.$locale.'" and
				MATCH (title, content) AGAINST ("'.$word.'" IN NATURAL LANGUAGE MODE)');
		if(!empty($pages_items)) {
			$global_count += count($pages_items);
			$pages_items_page = $pages_model->find();	// is the news page
			$result += array('pages' => $pages_items, 'pages_page' => $pages_items_page);
		}
		
		$result['global_count'] = $global_count;
		return $result;
	}
}

?>