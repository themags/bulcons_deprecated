<?php

class Page extends NestedSet {
	public
		$table_name = 'pages',
		$orig_table_name = 'pages',
        $module = 'pages',
        $file_extensions = array(
            'application/pdf' => 'pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx',
            'application/msword' => 'doc',
            'application/vnd.oasis.opendocument.text ' => 'odt'
        );
	protected
		$is_i18n = true,
		$has_mirror = false,

		$belongs_to = array(
            'albums' => array(
                'foreign_key'=>'gallery_id',
                'association_foreign_key'=>'id',
                'class_name' => 'AlbumsModel',
                'autocomplete_column' => 'title',
                'order'=>'ord ASC'
                ),
        ),

        $has_and_belongs_to_many = array(
            'news' => array(
                'foreign_key' => 'page_id',
                'association_foreign_key' => 'news_id',
                'class_name' => 'NewsModel',
                'join_table' => 'news_pages',
                'order' => 'date DESC'
            ),
        );
		protected $has_one = array(
            'file' => array(
                'association_foreign_key' => 'id',
                'foreign_key' => 'module_id',
                'class_name' => 'file_extra',
                'join_table' => 'pages',
                'conditions' => "module = 'pages' and keyname='file'"
            ),
		    'catalogue_file' => array(
		        'association_foreign_key' => 'id',
		        'foreign_key' => 'module_id',
		        'class_name' => 'catalogue_file_extra',
		        'join_table' => 'pages',
		        'conditions' => "module = 'pages' and keyname='catalogue_file'"
		    )
        );
	function __toString() {
		return $this->title;
	}

    /*function __get($k) {

        if($k=='slug') {
            return str_replace('//','/',Config()->COOKIE_PATH.$this->i18n_column_values[$k][$this->get_locale()]);
        }

        return parent::__get($k);
    }*/

	function before_validation() {
		parent::before_validation();
		if(($page = $this->find_by_slug($this->slug)) instanceOf Page && $page->id != $this->id) {
			$this->errors[] = Registry()->localizer->get_label('DB_FIELDS', 'slug')  . ' - ' . Registry()->localizer->get_label('DB_SAVE_ERRORS', 'already_exists');
		}

		$this->placeholders = 1;/// ne se polzva tova pole
	}

	function before_create() {
            parent::before_create();
            $page_type = array();
            $layout = array();
            $visibility = array();
            $title = array();
            $slug = array();

            foreach (Config()->LOCALE_SHORTCUTS as $value) {
                $page_type[$value] = $this->page_type;
                $layout[$value] = $this->layout;
                $visibility[$value] = 0;
                $title[$value] = $this->title;
                $slug[$value] = $this->slug;
            }

            $visibility[Registry()->locale] = $this->visibility;
            $this->visibility = $visibility;

            $this->page_type = $page_type;
            $this->layout = $layout;
            $this->title = $title;
            $this->slug = $slug;
        }

	function title_path() {
		$all = $this->get_parents();
		$result = array();

		foreach($all as $a) {
			if($a->title) {
				$result[] = $a->title;
			}
		}

		//array_pop($result);
		$result = array_reverse($result);

		return join(' / ',$result);
	}

    public function getSlugForContacts() {
        $contact_object = $this->find([
            'select' => 'pages_i18n.slug',
            'conditions' => 'page_type = "contact_us"',
            'limit' => 1
        ]);

        return $contact_object ? $contact_object->slug : '';
    }

	// function send_email
}

?>