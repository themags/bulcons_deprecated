<?php

class ShopsController extends ModulesController {
	public
		$module = 'shops',
		$models = 'ShopsModel';

	function index($params) {
		$obj = new $this->models[0];
		$list = new ListHelper($params);
		//$list->hide_default_main_actions = true;

		if($this->admin_helper->can('edit')) {
			$list->add_action('view', url_for(array('controller' => 'admin', 'module' => $this->module, 'maction' => 'edit', 'id' => ':id')));
		}

		if($this->admin_helper->can('delete')) {
			$list->add_action('delete', 'javascript:confirm_delete(:id);');
		}

		$list->add_column('id');
		$list->add_column('name');
		$list->add_column('city');
		$list->add_column('website');


		$list->add_filter('name', null, 'text');
		$list->add_filter('city', null, 'text');
		$list->add_filter('country', null, 'text');


		$items = $obj->find_all($list->to_sql(), 'id ASC', 30);
		$list->data($items);
		$this->render($list);
		$this->session->admin_return_to = $this->request->server('REQUEST_URI');
	}

	function edit($params){
    	$obj = $this->ShopsModel;
    	$object = $obj->find((int)$params['id']);
	    if($object) {
	    	if($this->is_post()) {
		        $response = new stdClass;
		        $response->ok = 0;
		        $response->errors = array();
		        $response->redirect_to = '';

		        if($object->save($_POST)) {
		          $this->log_action(array('id'=>$object->id,'message' => (string)$object));
		          $this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'edit');
		          $response->ok = 1;
		          $response->redirect_to = $this->session->admin_return_to;
		        } else {
	          		$response->errors = $object->errors;
		        }
		        $this->layout = 'json_save_callback';
		        $this->content_for_layout =  json_encode($response);
	     	 }

		      $this->form_object = $object;
		     /* $fake_model = new ProductsModel();
		      $package_model = new ProductsPackage();
		      d($package_model);*/
		      $this->view = 'admin_form';
			  $this->fields = $this->fields_helper->generate($this->module,$object);  
		    } else {
		     	$response->redirect_to = $this->session->admin_return_to;
		    }
	  }
}
?>
