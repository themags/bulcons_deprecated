<?php

class ShopsModel extends Modules {
	public
		$module = 'shops',
		$table_name = 'shops';
	protected $is_i18n = true;
	protected $has_mirror = true;

	public function find_cities_for_public($country_id = null) {
		$data = array();

		$result = self::$db->select('shops_i18n', 'DISTINCT(city)', "city != ''" . ($country_id ? " AND " . sprintf("country LIKE '%%%s%%'", Registry()->db->escape($country_id))  : ''), "ORDER BY city ASC");
		if($result->num_rows() > 0) {
			foreach ($result as $row) {
				$data[] = $row->city;
			}
		}

		return $data;
	}
}

?>