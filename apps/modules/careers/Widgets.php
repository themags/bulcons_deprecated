<?php

class CareersWidget extends BlockHelper {
	function index($block) {
		$model = new CareersModel;
		$params = unserialize($block->params);
		
		//ds();die;
		foreach($params['order'] as $k => $id) {
			if(!in_array($id, $params['items'])) {
				unset($params['order'][$k]);
				$block->save(array('params' => serialize($params)));
				continue;
			}

			$item = $model->find_by_id_and_active($id,1);
			if($item instanceOf $model) {
				$items[] = $item;
			}
		}

		foreach($params['items'] as $id) {
			if(!in_array($id, $params['order'])) {
				$params['order'][] = $id;
				$block->save(array('params' => serialize($params)));
			}
		}
		$pages = new Page();
		$page = $pages->find($block->page_id);
		$options = array(
			'partial' => __FUNCTION__ ,
			'items' => $items,
			'params' => $params,
			'page_id' => $block->page_id,
			'page' => $page,
			'block_id' => $block->id,
			'cache_hash' => $block->cache_hash,
			'smarty_cache_id' => __FUNCTION__ . '_offices_' . $block->id . '_' . Registry()->locale,
		);

		return $this->render_block($options);
	}

	function contact_form($block) {
		$params = unserialize($block->params);

		$options = array(
			'partial' => __FUNCTION__ ,
			'params' => $params,
			'page_id' => $block->page_id,
			'block_id' => $block->id,
			'cache_hash' => $block->cache_hash,
			'smarty_cache_id' => __FUNCTION__ . '_contact_us_' . $block->id . '_' . Registry()->locale,
		);

		return $this->render_block($options);
	}

	function sendRequest($params) {
		header('Content-type: application/json');

		$result = array();
		if(empty($params['block']) || (!empty($params['block']) && !($block = (new Block)->find_by_id_and_visibility($params['block'], 2)))) {
			$result = array('error' => 'invalid_block');
			goto result;
		}
		if(!$params['captcha_confirmation'] = Registry()->session->captcha) {
			$result = array('error' => Registry()->localizer->get_label('DB_SAVE_ERRORS', 'invalid_captcha'));
			goto result;
		}

		$model = new CareersModel;

		$block_params = unserialize($block->params);
		if(!empty($block_params['email_to']) && filter_var($block_params['email_to'], FILTER_VALIDATE_EMAIL)) {
			$model->email_to = $block_params['email_to'];
		} else {
			$result = array('error' => 'not_empty_email_to');
			goto result;
		}

		if($model->send_request($params)) {
			$result = array('success');
		} else {
			$result = array('errors' => $model->get_errors());
		}

		result:
		return json_encode($result);
	}
	
	function view($params) {
		header('Content-type: application/json');
	
		$object = null;
		$model = new NewsModel;
	
		if(!empty($params['slug_id']) && stristr($params['slug_id'], '+')) {
			list($slug, $id) = explode(' ', urldecode($params['slug_id']));
			$object = $model->find_by_id((int)$id, array('conditions' => "active=1 AND slug='" . Registry()->db->escape($slug) . "'"));
		}
	
		return $this->render_block(array(
				'params' => $params,
				'partial' => 'view',
				'object' => $object,
				'smarty_cache_id' => __FUNCTION__ . '_news_view_' . $params['slug_id'] . '_' . $_REQUEST['page'] . '_' . Registry()->locale,
		));
	}
}

?>