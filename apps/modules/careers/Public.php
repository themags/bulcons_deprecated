<?php
class CareersController extends ApplicationController {

	public $models = 'CareersModel Page';
	protected $layout = 'index';

	function view($params) {
		$this->careers = $this->Page->find(8);	
		//d($params);
		$model = new CareersModel;
		$this->obj = $model->find_by_slug_and_active($params['slug'],1);

		if(!$this->obj) {
			$this->redirect_to('/');
		}

		$response = new stdClass;
		$response->message = '';
		$response->errors = array();

		if ($this->is_post()) {
			/*d($_POST);
			d($_FILES);
			die;*/
			
			$this->form_object = new CareersRequestsModel;
		
			if ($this->form_object->customSave($_POST + array('ref' => $this->obj->ref, 'career_id' => $this->obj->id), $_FILES)) {
				$this->ok  = 1;
				$this->form_object->sendMailWithPosition();
			} else {
				//d($this->form_object->get_errors());
				$response->errors = $this->form_object->get_errors();
				return false;
			}
		}
		
	}
	
}
?>