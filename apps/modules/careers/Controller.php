<?php

class CareersController extends ModulesController {
	public
		$module = 'careers',
		$models = 'CareersModel CareersRequestsModel';

	function index($params) {

		$obj = new $this->models[0];
		$list = new ListHelper(true);

		if($this->admin_helper->can('edit')) {
			$list->add_action('edit', url_for(array('controller' => 'admin', 'module' => $this->module, 'maction' => 'edit', 'id' => ':id')));
		}

		if($this->admin_helper->can('delete')){
			$list->add_action('delete', 'javascript:confirm_delete(:id);');
		}

		$list->add_column('id');
		$list->add_column('ref');
		$list->add_column('title');
		$list->add_column('active');
		$list->add_main_action(array('link' => url_for(array('controller' => 'admin', 'module' => $this->module, 'maction' => 'request')), 'label' => 'request'));

		$items = $obj->find_all($list->to_sql(), 'ord ASC');
		$list->data($items);
		$this->render($list);

		$this->session->admin_return_to = $this->request->server('REQUEST_URI');
	}

	function request($params) {
		$obj = new $this->models[1];
		$career_model = new $this->models[0];
		$list = new ListHelper($params);

		if($this->admin_helper->can('view')) {
			$list->add_action('view', url_for(array('route_name' => 'chained_modules_admin', 'controller' => 'admin', 'module' => $this->module, 'maction' => 'view', 'id' => ':id')));
		}
		//use this for custom delete 
		$this->delete_action = 'delete_request';
		$list->add_action('delete', 'javascript:confirm_delete(:id);');

		$list->add_column('id');
		$list->add_column('ref');
		$list->add_column('title');
		$list->add_column('name');
		$list->add_column('email');
		$list->add_column('is_viewed');
		$list->add_column('created_on');
		$list->hide_default_main_actions = true;

		$title_values = $this->_load_titles();
		$list->add_filter('title', $title_values, 'select');
		$list->add_main_action(array('link' => url_for(array('controller' => 'admin', 'module' => $this->module, 'maction' => 'index')), 'label' => ($this->localizer->get_label('MAIN_ACTIONS', 'careers'))));
		$conditions = $list->to_sql();
		if (strstr($conditions, 'title')) {
			$conditions = str_replace('title', 'ref', $conditions);
		}
		$items = $obj->find_all($conditions, null, 15);
		foreach ($items as &$item) {
			$item->title = $career_model->find_by_ref($item->ref)->title;
		}
		$list->data($items);
		$this->render($list);

		$this->session->admin_return_to = $this->request->server('REQUEST_URI');
	}

	function delete_request($params){
		//use this for custom delete 
		$object = $this->CareersRequestsModel->find((int)$params['id']);

		if($object) {
			$object->delete();
			$this->log_action(array('id'=>$object->id,'message' => (string)$object));
			$this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'delete');

		} 

		$this->redirect_to(url_for(array(
			'route_name' => 'chained_modules_admin',
			'controller' => 'admin',
			'module' => $this->module,
			'maction' => 'request'
		)));
	}
	
	function _load_titles() {
		$model = new $this->models[0];
		$items = $model->find_all();
		$result = [];
		foreach($items as &$item) {
			$result[$item->ref] = $item->title;
		}
		return $result;
	}
	function getList_created_on($v) {
		
		return $this->_time_elapsed_string($v);
	}

	function view($params) {
		$this->object = $this->CareersRequestsModel->find(intval($params['id']));

		if( ! $this->object instanceof CareersRequestsModel ) {
        	$this->error404();
      	}

		if ($this->object->is_viewed == 0) {
			$this->object->is_viewed = 1;
			$this->object->save();
		}
		$this->career = $this->object->career();
	}

	function getList_is_viewed($v) {
		return $this->localizer->get('yesno',(int)$v);
	}
	public function getList_title($value, $object) {
		if( $object instanceof CareersRequestsModel && $object->career instanceof CareersModel ) {
			return $object->career->title;
		}
		elseif( $object instanceof CareersModel ) {
			return $object->title;
		}

		return 'Без позиция'; // just in case
	}

	protected function save_order($params) {
		CareersModel::updateOrdering(explode(',', $_POST['ids']));
		$this->log_action();
		echo $this->localizer->get_label('KEYWORDS', 'order_saved');
	}
	
	function _time_elapsed_string($ptime) {
		$etime = time() - strtotime($ptime);
		if ($etime < 1) {
			return '0 seconds';
		}
	
		$a = array( 365 * 24 * 60 * 60  =>  'year',
				30 * 24 * 60 * 60  =>  'month',
				24 * 60 * 60  =>  'day',
				60 * 60  =>  'hour',
				60  =>  'minute',
				1  =>  'second'
		);
		$a_plural = array( 'year'   => 'years',
				'month'  => 'months',
				'day'    => 'days',
				'hour'   => 'hours',
				'minute' => 'minutes',
				'second' => 'seconds'
		);
	
		foreach ($a as $secs => $str)
		{
			$d = $etime / $secs;
			if ($d >= 1)
			{
				$r = round($d);
				return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
			}
		}
	}
}

?>