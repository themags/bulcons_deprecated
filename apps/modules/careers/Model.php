<?php

	class CareersModel extends Modules {
		public
			$module = 'careers',
			$table_name = 'careers';
		protected $is_i18n = true;
	    protected $has_mirror = true;
	}

class CareersRequestsModel extends Modules {
	public
		$module = 'careers_requests',
		$table_name = 'careers_requests',
		$file_extensions = array(
			'application/pdf' => 'pdf',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx',
			'application/msword' => 'doc',
			'application/vnd.oasis.opendocument.text ' => 'odt'
	);
	protected
		$is_i18n = false,
		$has_mirror = false,
		$has_one = array(
			'cv' => array(
				'association_foreign_key' => 'id', 
				'foreign_key' => 'module_id', 
				'class_name' => 'file', 
				'join_table' => 'files', 
				'is_required' => false, 
				'conditions' => "module = 'careers_requests' and keyname='upload_cv'"
			),
			'cl' => array(
				'association_foreign_key' => 'id', 
				'foreign_key' => 'module_id', 
				'class_name' => 'file', 
				'join_table' => 'files', 
				'is_required' => false, 
				'conditions' => "module = 'careers_requests' and keyname='upload_cl'"
			),
		),
		$belongs_to = array(
			'career' => array(
				'association_foreign_key' => 'id', 
				'foreign_key' => 'career_id', 
				'class_name' => 'CareersModel'
			)
		);
	private
		$_files,
		$_files_info,
		$_attached_files_directory = 'careers_requests';
	
	function sendMailWithPosition() { 

        Registry()->tpl->assign('form_object', $this);
        $text = Registry()->tpl->fetch(Config()->VIEWS_PATH.'/public/partials/_career_email_with_position.htm'); 
        $root_path = Config()->FILES_ROOT; 

		$cv = $this->getCvFilepath();
		$cv_filename = $this->cv->filename;

		$cl = $this->getClFilepath();
		$cl_filename = $this->cl->filename;

        $cv_filepath = sprintf(
        	'%s%s',
    		$root_path,
    		$cv
		);

		$cl_filepath = sprintf(
			'%s%s',
			$root_path,
			$cl
		);

		if(send_php_mail(array(
			'from' => array('Bulcons', 'web@bulcons.com'),
			'mail' => Registry()->localizer->get_label('EMAIL_LABELS','career_mail_to'),
			'subject' => Registry()->localizer->get_label('EMAIL_LABELS','careers_request_send'),
			'html' => $text,
			'attachments' => array(
				array('path' => $cv_filepath, 'name' => $cv_filename),
				array('path' => $cl_filepath, 'name' => $cl_filename)
				)
		))) {
			return true;
		} else {
			return false;
		}
    } 
    function sendMailWithoutPosition() { 

        Registry()->tpl->assign('form_object', $this);
        $text = Registry()->tpl->fetch(Config()->VIEWS_PATH.'/public/partials/_career_email_without_position.htm'); 
        $root_path = Config()->FILES_ROOT; 

		$cv = $this->getCvFilepath();
		$cv_filename = $this->cv->filename;

        $cv_filepath = sprintf(
        	'%s%s',
    		$root_path,
    		$cv
		);


		if(send_php_mail(array(
			'from' => array('Bulcons', 'web@bulcons.com'),
			'mail' => Registry()->localizer->get_label('EMAIL_LABELS','career_mail_to'),
			'subject' => Registry()->localizer->get_label('EMAIL_LABELS','careers_request_send'),
			'html' => $text,
			'attachments' => array(
				array('path' => $cv_filepath, 'name' => $cv_filename),
				)
		))) {
			return true;
		} else {
			return false;
		}
    } 
	
	function before_validation() {	
	//d($this->_files);	
		if(!empty($this->_files)) {
			$uploaded_files = [];
			foreach ($this->_files as $key => $file) {
				if(is_uploaded_file($file['tmp_name'])) {
					$uploaded_files[$key] = $file;
					if(!Files::mime_content_type($file['tmp_name'], 'documents')) {					
						$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'inavalid_mime_type_document')), $key);
					}
				}else{
					$this->add_error(Inflector::humanize(Registry()->localizer->get_label('DB_SAVE_ERRORS', 'not_empty')), $key);
				}
			}$this->_files = $uploaded_files;			
		}

		return parent::before_validation();
	}

	function after_validation_on_create() {
		//d($_POST);
		// Validates common data for both forms.
		if( empty($_POST['capture']) ) {
                $this->add_error('not_empty', 'capture');
        }

        if ($_POST['capture'] == Registry()->session->captcha){
            unset($_POST['capture']);
        } else {
            $this->add_error('invalid_captcha', 'capture');
        }

        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $this->add_error('invalid_email', 'email');
        }

        // Custom validations
		if ($this->career_id) {
			$this->validateDataForCareer();
		}
		else {
			$this->validateData();
		}

		//d(Registry()->session->captcha);
		
		return parent::after_validation_on_create();
	}

	/**
	 * Validates user data without career object
	 */
	private function validateData() {
		//d($_POST);die;
		if(empty($_POST['message'])) {
			$this->add_error('not_empty', 'message');
		}
	}

	/**
	 * Validates user data for specific career
	 */
	private function validateDataForCareer() {
		// validates phone
		if(empty($_POST['phone'])) {
			$this->add_error('not_empty', 'phone');
		}
	}
	
	function customSave($attributes, $files) {
		if(!empty($files)) {
			$this->_files = $files;
		}
		return self::save($attributes);
	}
	
	function after_save() {
		if(!empty($this->_files)) {
			$directory = $this->get_attached_files_directory();
			foreach ($this->_files as $key => $file) {
				$extension = pathinfo($file['name'], PATHINFO_EXTENSION);
				$files_table = new File;
				$files_table->save(['module_id' => $this->id, 'module' => $this->_attached_files_directory, 'keyname' => $key, 'filename' => $file['name'], 'filesize' => $file['size'], 'ord' => 1], true);
			}
		}
	}
	function get_attached_files_directory() {
		return Config()->FILES_ROOT . $this->_attached_files_directory;
	}

	public function getCvFilepath() {
		$cv_object = $this->cv();

		if ($cv_object) {
			return sprintf(
				'%s/%d/%s/%s',
				$cv_object->module,
				$cv_object->module_id,
				$cv_object->keyname,
				$cv_object->filename
			);
		}

		return '';
	}

	public function getClFilepath() {
		$cl_object = $this->cl();

		if ($cl_object) {
			return sprintf(
				'%s/%d/%s/%s',
				$cl_object->module,
				$cl_object->module_id,
				$cl_object->keyname,
				$cl_object->filename
			);
		}

		return '';
	}
	
}

?>
