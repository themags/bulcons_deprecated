<?php

class UsersController extends ModulesController {
	public
		$module = 'users',
		$models = 'UsersModel';

	function index($params) {
		$obj = new $this->models[0];
		$list = new ListHelper($params);
		//$list->hide_default_main_actions = true;

		if($this->admin_helper->can('edit')) {
			$list->add_action('view', url_for(array('controller' => 'admin', 'module' => $this->module, 'maction' => 'edit', 'id' => ':id')));
		}

		if($this->admin_helper->can('edit')) {
			$list->add_action('mail', url_for(array('controller' => 'admin', 'module' => $this->module, 'maction' => 'send_mail', 'id' => ':id')));
		}

		$list->add_action('delete', 'javascript:confirm_delete(:id);');

		$list->add_column('distributor_name');
		$list->add_column('email');

		$list->add_filter('city', null, 'text');
		$list->add_filter('country', null, 'text');


		$items = $obj->find_all($list->to_sql(), 'created_at DESC', 30);
		$list->data($items);
		$this->render($list);
		$this->session->admin_return_to = $this->request->server('REQUEST_URI');
	}


	function edit($params){
    	$obj = $this->UsersModel;
    	$object = $obj->find((int)$params['id']);
	    if($object) {
	    	if($this->is_post()) {
		        $response = new stdClass;
		        $response->ok = 0;
		        $response->errors = array();
		        $response->redirect_to = '';

		        if($object->save($_POST)) {
		          $this->log_action(array('id'=>$object->id,'message' => (string)$object));
		          $this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'edit');
		          $response->ok = 1;
		          $response->redirect_to = $this->session->admin_return_to;
		        } else {
	          		$response->errors = $object->errors;
		        }
		        $this->layout = 'json_save_callback';
		        $this->content_for_layout =  json_encode($response);
	     	 }

		      $this->form_object = $object;
		      $this->view = 'admin_form';
			  $this->fields = $this->fields_helper->generate($this->module,$object);  
		    } else {
		     	$response->redirect_to = $this->session->admin_return_to;
		    }
	}

	function send_mail($params) {
		d($params);
		$obj = $this->UsersModel;
		$object = $obj->find((int)$params['id']);
		d($object);
		die('tuka e taka ');
            //make template here for sending username and password
            Registry()->tpl->assign('obj', $object);
            $from = array(Registry()->localizer->get_label('CONTACT_FORM','from_email'),Registry()->localizer->get_label('CONTACT_FORM','from_email'));
            $mail = Registry()->localizer->get_label('CONTACT_FORM','to_email');
            $text = Registry()->tpl->fetch(Config()->VIEWS_PATH.'/public/partials/_contact_email.htm'); 
            $subject = Registry()->localizer->get_label('CONTACT_FORM','contact_form_send');

                if(send_php_mail(array(
                    'from' => $from,
                    'mail' => $mail,
                    'subject' => $subject,
                    'html' => $text
                ))) {
                	$response->redirect_to = $this->session->admin_return_to;
                    return true;
                } else {
                	// add error
                    return false;
                }
        }
}
?>
