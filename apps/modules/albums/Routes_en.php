<?php
$p = new Page();
$pages = $p->find_all('page_type="gallery"');
foreach($pages as $pp) {
    Router()->add('gallery', $pp->slug.'/:slug', array('controller' => 'albums', 'app'=>'albums', 'action' => 'view', 'slug' => COMPULSORY));
}
?>