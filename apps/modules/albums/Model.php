<?php

    class AlbumsModel extends Modules { 
        
        public $module = 'albums';
        protected $is_i18n = true;
        protected $has_mirror = true;
        public $table_name = 'albums';

        public $image_extensions = array(
            'image/jpeg'    => 'jpg',
            'image/jpg'     => 'jpg',
            'image/pjpeg'   => 'jpg',
            'image/gif'     => 'gif',
            'image/png'     => 'png'
        );
        protected $has_many = array(
            'images' => array(
                'association_foreign_key'=>'id',
                'foreign_key' => 'module_id', 
                'class_name'=>'image', 
                'join_table' => 'images', 
                'conditions' => "module = 'albums' and keyname='images'",
                'order' => 'ord ASC'
            ),
        );
    }
?>