<?php
    class AlbumsController extends ApplicationController
    {
        public $models = 'Page';
        function view($params)
        {
            $model = new AlbumsModel;

            $this->item = $model->find_by_active_and_slug(1, $params['slug']);
            if(!$this->item) {
                $this->redirect_to('/');
            }

            $this->obj = $this->Page->find($this->item->page_id);
        }
    }
?>