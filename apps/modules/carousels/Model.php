<?php

    class CarouselsModel extends Modules { 
        
        public $module = 'carousels';
        protected $is_i18n = true;
        protected $has_mirror = true;
        public $table_name = 'carousels';

        public $image_extensions = array(
            'image/jpeg'    => 'jpg',
            'image/jpg'     => 'jpg',
            'image/pjpeg'   => 'jpg',
            'image/gif'     => 'gif',
            'image/png'     => 'png'
        );


        protected $has_one = array(
            'image' => array(
                'association_foreign_key'=>'id', 
                'foreign_key' => 'module_id', 
                'class_name'=>'image', 
                'join_table' => 'images', 
                'conditions' => "module = 'carousels' and keyname='image'"),
        );
    }
?>