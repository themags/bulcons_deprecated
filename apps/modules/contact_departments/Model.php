<?php

    class ContactDepartmentsModel extends Modules { 
        
        public $module = 'contact_departments';
        protected $is_i18n = true;
        protected $has_mirror = true;
        public $table_name = 'contact_departments';

        protected $belongs_to = array(
            'contact_departments_category' => array(
                'association_foreign_key'=>'id', 
                'foreign_key' => 'contact_departments_category_id', 
                'class_name'=>'ContactDepartmentsCategory', 
                'join_table' => 'contact_departments_categories', 
                'column'=>'category_title'
            )
        );
    }

    class ContactDepartmentsCategory extends Modules {
        public $module = 'contact_departments';
        protected $is_i18n = true;
        protected $has_mirror = true;
        public $table_name = 'contact_departments_categories';

        protected
            $has_many = array(
                'contacts' => array(
                    'class_name' => 'ContactDepartmentsModel',
                    'foreign_key' => 'contact_departments_category_id',
                    'association_foreign_key' => 'id',
                    'conditions' => 'active = 1',
                )
            );

    }


?>