<?php 

class ContactDepartmentsController extends ModulesController {

  public $module = 'contact_departments';
  public $models = 'ContactDepartmentsModel ContactDepartmentsCategory';

  function index($params) {
    $obj = new $this->models[0];
    $params['sortable'] = true;
    $list = new ListHelper($params);
    if($this->admin_helper->can('edit')){
      $list->add_action('edit', url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'edit', 'id' => ':id')));
    }
    if($this->admin_helper->can('delete')){
      $list->add_action('delete', 'javascript:confirm_delete(:id);');
    }
    $list->add_column('id');
    $list->add_column('title');
    $list->add_column('contact_departments_category_id');
   
    $list->add_column('active');

    
    $categories = $this->ContactDepartmentsCategory->find_all();
    foreach ($categories as $cat) {
      $this->categories[$cat->id] = $cat->category_title;
    }

   
    
    $list->add_filter('category_title','null','text');
    $list->add_filter('contact_departments_category_id', $this->categories, 'select');
   
    $list->add_filter('active', $this->localizer->get('yesno'), 'select');

    $items = $obj->find_all($list->to_sql(), 'ord ASC');    
    $list->hide_main_actions = false;
    $list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'index')),'label'=>'index'));
   
    $list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'categories')),'label'=>'categories'));

    $list->data($items);
    $this->render($list);
    $this->session->admin_return_to = $this->request->server('REQUEST_URI');
  }

  function getList_contact_departments_category_id($v){
      return $this->categories[$v];
  }

  /* Categories */
  
  function categories($params) {
    $list = new ListHelper(true);
    if($this->admin_helper->can('edit')){
      $list->add_action('edit', url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'edit_category', 'id' => ':id')));
    }
    if($this->admin_helper->can('delete_category')){
      $list->add_action('delete', url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'delete_category', 'id' => ':id')));
    }
    $list->add_column('id');
    $list->add_column('category_title');

    $items = $this->ContactDepartmentsCategory->find_all($list->to_sql(), 'ord ASC');    
    $list->hide_default_main_actions = true;
    $list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'add_category')),'label'=>'add')); 
  
    $list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'categories')),'label'=>'categories'));
    $list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'index')),'label'=>'contact_departments'));

    $list->data($items);            
    $this->render($list);
    $this->session->admin_return_to = $this->request->server('REQUEST_URI');
  }


  function add_category($params){ 
    if($this->is_post()){
      $obj = $this->ContactDepartmentsCategory;
      $response = new stdClass;
      $response->ok = 0;
      $response->errors = array();
      $response->redirect_to = '';

      if($obj->save($_POST)) {
        $this->handle_uploads($obj, $this->module);
        $this->log_action(array('id'=>$obj->id,'message' => (string)$obj));
        $this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'add');
        $response->ok = 1;
        $response->redirect_to = $this->session->admin_return_to;
      } else {
        $response->errors = $obj->errors;
      }
      $this->layout = 'json_save_callback';
      $this->content_for_layout =  json_encode($response);
    } else {
      $this->view = 'admin_form';
      $this->fields = $this->fields_helper->generate($this->module,$this->ContactDepartmentsCategory);      
    }
  }

  function edit_category($params){
    $obj = $this->ContactDepartmentsCategory;
    $object = $obj->find((int)$params['id']);
    if($object) {
      if($this->is_post()) {
        $response = new stdClass;
        $response->ok = 0;
        $response->errors = array();
        $response->redirect_to = '';

        if($object->save($_POST)) {
          $this->log_action(array('id'=>$object->id,'message' => (string)$object));
          $this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'edit');
          $response->ok = 1;
          $response->redirect_to = $this->session->admin_return_to;
        } else {
          $response->errors = $object->errors;
        }
        $this->layout = 'json_save_callback';
        $this->content_for_layout =  json_encode($response);
      }

      $this->form_object = $object;
      $this->view = 'admin_form';
      $this->fields = $this->fields_helper->generate($this->module,$object);  
    } else {
      $response->redirect_to = $this->session->admin_return_to;
    }
  }

  function delete_category($params){
    $object = $this->ContactDepartmentsCategory->find((int)$params['id']);

    if($object) {
      $object->delete();
      $this->log_action(array('id'=>$object->id,'message' => (string)$object));
      $this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'delete');

    } 
    $this->redirect_to($this->session->admin_return_to);
  }

  protected function save_order_categories($params){
      $ids = explode(',', $_POST['ids']);
      foreach ($ids as $key => $id){
          Registry()->db->query('UPDATE '.$this->ContactDepartmentsCategory->table_name.' SET ord='.(int)$key.' WHERE id='.$id.'');
      }
      $this->log_action();
      echo $this->localizer->get_label('KEYWORDS', 'order_saved');
  }
}
?>