<?php

    class CertificationsModel extends Modules { 
        
        public $module = 'certifications';
        protected $is_i18n = true;
        protected $has_mirror = true;
        public $table_name = 'certifications';

       public $image_extensions = array(
            'image/jpeg'    => 'jpg',
            'image/jpg'     => 'jpg',
            'image/pjpeg'   => 'jpg',
            'image/gif'     => 'gif',
            'image/png'     => 'png'
        );
        protected $has_one = array(
            'image' => array(
                'association_foreign_key'=>'id', 
                'foreign_key' => 'module_id',
                'is_required' => true, 
                'class_name'=>'image_extra', 
                'join_table' => 'images_exta', 
                'conditions' => "module = 'certifications' and keyname='image'"),
        ); 
	}
?>