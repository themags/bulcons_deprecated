<?php
	
	$page = new Page;
	$obj = $page->find_by_page_type('recipes');

	$fake = new RecipesModel;
	$category = new RecipesCategory;
	$recipes = $fake->find_all('active=1');
	foreach($recipes as $a)
	{
		Router()->add(
			'recipie_inner',
			$obj->slug.'/'.$a->recipes_category->slug.'/'.$a->slug,
			array(
				'app'=>'recipes',
				'controller' => 'recipes',
				'action'=>'view_recipe',
				'slug' => $a->slug,
				'page_id'=>$obj->id,
				'id' => $a->recipes_category->id
			)
		);
	}

	$all = $category->find_all(null,'ord ASC');
	foreach($all as $a)
	{
		Router()->add(
			'recipie_category', 
			$obj->slug.'/'.$a->slug, 
			array(
				'controller'=>'recipes',
				'app'=>'recipes', 
				'action'=>'view_category', 
				'id' => $a->id, 
				'page_id'=>$obj->id, 
				'slug' => $a->slug
				)
			);
	}
?>