<?php
class RecipesController extends ApplicationController {

    public $models = 'RecipesModel Page';
    protected $layout = 'index';

    function view_category($params) {   
        $fake = new RecipesModel;
        $category = new RecipesCategory;
        $this->recipie = $category->find((int)$params['id']);
        if(!$this->recipie) {
            $this->redirect_to('/');
        }
        $this->subcategories = $category->find_all(null,'ord ASC');
        $this->obj = $this->Page->find((int)$params['page_id']);
        $this->recipes_list = $fake->find_all_by_recipes_category_id_and_active((int)$params['id'] , 1, array('limit' => 8, 'order' => 'ord ASC'));
        $this->paginator = new Paginator(current($this->recipes_list)->pages, $_GET['page']);
    }
    
    function view_recipe($params) {
        $fake = new RecipesModel;
        $category = new RecipesCategory;
        $this->recipie = $category->find((int)$params['id']);
/*
        if(!$this->recipie) {
            $this->redirect_to('/');
        }*/
        // ako go nqma - redirect ..
        $this->product_page_slug = $this->Page->find('9')->slug;
        $this->subcategories = $category->find_all(null,'ord ASC');
        $this->obj = $this->Page->find((int)$params['page_id']);
        //d($this->recipie->slug);
        $this->recipe_item = $fake->find_by_slug_and_active($params['slug'] , 1);
        //d($this->recipe_item->title);
        $this->url = Config()->BASE_URL.Registry()->request->request_uri;
        $this->title = $this->recipe_item->title;



        $this->og_info['type'] = 'article';
        $this->og_info['title'] = $this->recipe_item->title;
        $this->og_info['description'] =  $this->recipe_item->summary;
        $this->og_info['url'] = $this->url;
        $this->og_info['image'] = Config()->BASE_URL.Config()->PUBLIC_URL.'files/'.$this->recipe_item->module.'/'.$this->recipe_item->id.'/'.$this->recipe_item->picture->keyname.'/'.$this->recipe_item->picture->filename;
        //$this->fb_share = Config()->BASE_URL.Registry()->request->request_uri; 
    }
}
?>