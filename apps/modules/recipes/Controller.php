<?php 

class RecipesController extends ModulesController {

  public $module = 'recipes';
  public $models = 'RecipesModel RecipesCategory';

  function index($params) {
    $obj = new $this->models[0];
    $params['sortable'] = true;
    $list = new ListHelper($params);
    if($this->admin_helper->can('edit')){
      $list->add_action('edit', url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'edit', 'id' => ':id')));
    }
    if($this->admin_helper->can('delete')){
      $list->add_action('delete', 'javascript:confirm_delete(:id);');
    }
    $list->add_column('id');
    $list->add_column('title');
    $list->add_column('recipes_category_id');
    $list->add_column('active');
    $list->add_column('is_on_focus');

    
    $categories = $this->RecipesCategory->find_all();
    foreach ($categories as $cat) {
      $this->categories[$cat->id] = $cat->title;
    }
    
    $list->add_filter('title','null','text');
    $list->add_filter('recipes_category_id', $this->categories, 'select');
    $list->add_filter('active', $this->localizer->get('yesno'), 'select');

    $items = $obj->find_all($list->to_sql(), 'ord ASC');    
    $list->hide_main_actions = false;
    $list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'index')),'label'=>'index'));
    $list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'categories')),'label'=>'categories'));
    
    $list->data($items);
    $this->render($list);
    $this->session->admin_return_to = $this->request->server('REQUEST_URI');
  }

  function getList_is_on_focus($v){
    return $this->localizer->get('yesno',(int)$v);
  }

  function getList_recipes_category_id($v){
      return $this->categories[$v];
  }

  function getList_is_viewed($v){
     return $this->localizer->get('yesno',(int)$v);
  }

  /* Categories */
  
  function categories($params) {
    $list = new ListHelper(true);
    if($this->admin_helper->can('edit')){
      $list->add_action('edit', url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'edit_category', 'id' => ':id')));
    }
    if($this->admin_helper->can('delete_category')){
      $list->add_action('delete', url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'delete_category', 'id' => ':id')));
    }
    $list->add_column('id');
    $list->add_column('title');

    $items = $this->RecipesCategory->find_all($list->to_sql(), 'ord ASC');    
    $list->hide_default_main_actions = true;
    $list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'add_category')),'label'=>'add')); 
    $list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'index')),'label'=>'index'));
    $list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'categories')),'label'=>'categories'));

    $list->data($items);            
    $this->render($list);
    $this->session->admin_return_to = $this->request->server('REQUEST_URI');
  }

  function getList_image($v) {
    return '<img src="'.$v.'" />';
  }

  function add_category($params){ 
    if($this->is_post()){
      $obj = $this->RecipesCategory;
      $response = new stdClass;
      $response->ok = 0;
      $response->errors = array();
      $response->redirect_to = '';

      if($obj->save($_POST)) {
        $this->handle_uploads($obj, $this->module);
        $this->log_action(array('id'=>$obj->id,'message' => (string)$obj));
        $this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'add');
        $response->ok = 1;
        $response->redirect_to = $this->session->admin_return_to;
      } else {
        $response->errors = $obj->errors;
      }
      $this->layout = 'json_save_callback';
      $this->content_for_layout =  json_encode($response);
    } else {
      $this->view = 'admin_form';
      $this->fields = $this->fields_helper->generate($this->module,$this->RecipesCategory);      
    }
  }

  function edit_category($params){
    $obj = $this->RecipesCategory;
    $object = $obj->find((int)$params['id']);
    if($object) {
      if($this->is_post()) {
        $response = new stdClass;
        $response->ok = 0;
        $response->errors = array();
        $response->redirect_to = '';

        if($object->save($_POST)) {
          $this->log_action(array('id'=>$object->id,'message' => (string)$object));
          $this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'edit');
          $response->ok = 1;
          $response->redirect_to = $this->session->admin_return_to;
        } else {
          $response->errors = $object->errors;
        }
        $this->layout = 'json_save_callback';
        $this->content_for_layout =  json_encode($response);
      }

      $this->form_object = $object;
      $this->view = 'admin_form';
      $this->fields = $this->fields_helper->generate($this->module,$object);  
    } else {
      $response->redirect_to = $this->session->admin_return_to;
    }
  }

  function delete_category($params){
    $object = $this->RecipesCategory->find((int)$params['id']);

    if($object) {
      $object->delete();
      $this->log_action(array('id'=>$object->id,'message' => (string)$object));
      $this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'delete');

    } 
    $this->redirect_to($this->session->admin_return_to);
  }

  protected function save_order_categories($params){
      $ids = explode(',', $_POST['ids']);
      foreach ($ids as $key => $id){
          Registry()->db->query('UPDATE '.$this->RecipesCategory->table_name.' SET ord='.(int)$key.' WHERE id='.$id.'');
      }
      $this->log_action();
      echo $this->localizer->get_label('KEYWORDS', 'order_saved');
  }

}
?>