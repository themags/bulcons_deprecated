<?php

    class RecipesModel extends Modules {

        public $module = 'recipes';
        protected 
            $is_i18n = true,
            $has_mirror = true;
        public $table_name = 'recipes';

        public $image_extensions = array(
                'image/jpeg'    => 'jpg',
                'image/jpg'     => 'jpg',
                'image/pjpeg'   => 'jpg',
                'image/gif'     => 'gif',
                'image/png'     => 'png'
        );

        protected $has_one = array(
                'picture' => array(
                    'association_foreign_key'=>'id', 
                    'foreign_key' => 'module_id', 
                    'is_required' => false,
                    'class_name'=>'image', 
                    'join_table' => 'images', 
                    'conditions' => "module = 'recipes' and keyname='picture'")


        );

        protected $has_many = array(
                'images' => array(
                    'association_foreign_key' => 'id', 
                    'foreign_key' => 'module_id', 
                    'is_required' => false,
                    'class_name' => 'image', 
                    'join_table' => 'images', 
                    'conditions' => "module = 'recipes' and keyname='images'"),
        );

        protected $has_and_belongs_to_many = array(
                'products' => array(
                    'association_foreign_key' => 'product_id', 
                    'foreign_key' => 'recipe_id',
                    'class_name' => 'ProductsModel',
                    'join_table' => 'recipes_products',
                    'autocomplete_column' => 'title',
                ),
         );

        protected $belongs_to = array(
            'recipes_category' => array(
                'association_foreign_key'=>'id', 
                'foreign_key' => 'recipes_category_id', 
                'class_name'=>'RecipesCategory', 
                'join_table' => 'recipes_category', 
                'autocomplete_column'=>'title')

        );

        function after_save (){
            if($this->is_on_focus == 1){
                $this->update_all('is_on_focus = 0', 'id !=' . $this->id);
            }
            return parent::after_save();
        }
    }

    class RecipesCategory extends Modules {
        public $module = 'recipes';
        protected $is_i18n = true;
        protected $has_mirror = true;
        public $table_name = 'recipes_categories';
    }

?>