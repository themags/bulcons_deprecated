<?php

    class ContactsModel extends Modules { 
        
        public $module = 'contacts';
        protected $is_i18n = true;
        protected $has_mirror = true;
        public $table_name = 'contacts';
        
        public $image_extensions = array(
        		'image/jpeg'    => 'jpg',
        		'image/jpg'     => 'jpg',
        		'image/pjpeg'   => 'jpg',
        		'image/gif'     => 'gif',
        		'image/png'     => 'png'
        );
}

    class ContactRequestModel extends Modules {

        public $module = 'contact_requests';
        protected $is_i18n = false;
        public $table_name ='contact_requests';

        public function after_validation_on_create(){     
            if(empty($_POST['capture'])) {
                $this->add_error(Registry()->localizer->get_label('DB_SAVE_ERRORS','not_empty'), 'capture');
            }elseif($_POST['capture'] == Registry()->session->captcha){
                unset($_POST['capture']);
            }else {
                $this->add_error(Registry()->localizer->get_label('DB_SAVE_ERRORS','invalid_captcha'), 'capture');
            }
            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $this->add_error(Registry()->localizer->get_label('DB_SAVE_ERRORS','invalid_email'), 'email');
            }

            return parent::after_validation_on_create();
        }
        public function after_create(){
            $this->sendMail();
            parent::after_create();
        }

        function sendMail() {
            //make template here
            Registry()->tpl->assign('form_object', $this);
            $from = array('Bulcons', 'web@bulcons.com');
            $mail = Registry()->localizer->get_label('EMAIL_LABELS','contact_mail_to');
            $text = Registry()->tpl->fetch(Config()->VIEWS_PATH.'/public/partials/_contact_email.htm'); 
            $subject = Registry()->localizer->get_label('EMAIL_LABELS','contact_request_send');

                if(send_php_mail(array(
                    'from' => $from,
                    'mail' => $mail,
                    'subject' => $subject,
                    'html' => $text
                ))) {
                    return true;
                } else {
                    return false;
                }
        }

        public function before_validation() {
            if (!isset($_POST['gdpr'])) {
                $this->add_error('Cannot be unchecked', 'gdpr');
            }

            return parent::before_validation();
        }
    }
?>
