<?php 

class ContactsController extends ModulesController {

  public $module = 'contacts';
  public $models = 'ContactsModel ContactRequestModel';

  function index($params) {

    $obj = new $this->models[0];
    $params['sortable'] = true;
    $list = new ListHelper($params);
    $list->add_action('edit', url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'edit', 'id' => ':id')));
    $list->add_action('delete', 'javascript:confirm_delete(:id);');
    $list->add_column('id');
    $list->add_column('email');
    $list->add_column('address');
    $list->add_column('active');
    


    $list->add_filter('address','null','text');
    $list->add_filter('active', $this->localizer->get('yesno'), 'select');
    $count = $obj->count_all();
    
    if ($count >= 1){
      $list->hide_default_main_actions = true;
    }
    $list->hide_main_actions = false;
    $items = $obj->find_all($list->to_sql(), 'ord ASC');    
    $list->add_main_action(array('link' => url_for(array('controller' => 'admin', 'module' => $this->module, 'maction' => 'request')), 'label' => 'request'));
    //$items = $obj->find_all($list->to_sql(), 'created_at ASC');    
    $list->data($items);
    $this->render($list);
    $this->session->admin_return_to = $this->request->server('REQUEST_URI');
  }

  function request ($params){

    $obj = new $this->models[1];
    $list = new ListHelper($params);

    if($this->admin_helper->can('view')) {
      $list->add_action('view', url_for(array('route_name' => 'chained_modules_admin', 'controller' => 'admin', 'module' => $this->module, 'maction' => 'view', 'id' => ':id')));
    }
    //custom delete
    $this->delete_action = 'delete_request';
    $list->add_action('delete', 'javascript:confirm_delete(:id);');

    $list->hide_default_main_actions = true;
    $list->add_column('id');
    $list->add_column('name');
    $list->add_column('email');
    $list->add_column('created_at');
    $list->add_column('reviewed');

    $list->add_filter('name','null','text');
    $list->add_filter('reviewed', $this->localizer->get('yesno'), 'select');
    $list->add_main_action(array('link' => url_for(array('controller' => 'admin', 'module' => $this->module, 'maction' => 'index')), 'label' => 'contacts'));
    $items = $obj->find_all($list->to_sql(), 'created_at DESC');    
    $list->data($items);
    $this->render($list);
    $this->session->admin_return_to = $this->request->server('REQUEST_URI');
  }

  function view($params) {
    $obj = new $this->models[1];
    
    if (!$this->fields = $obj->find($params['id'])){
      $this->error404();
    }
    
    if ($this->fields->reviewed == 0) {
        $this->fields->reviewed = 1;
        $this->fields->save();
    }
    //$this->career = $this->fields->career();
    //$this->fields->cv = $this->fields->cv();
  }

  public function getList_reviewed($v){
    return $this->localizer->get('yesno', (int)$v);
  }

  function delete_request($params){
    //use this for custom delete 
    $object = $this->ContactRequestModel->find((int)$params['id']);

    if($object) {
      $object->delete();
      $this->log_action(array('id'=>$object->id,'message' => (string)$object));
      $this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'delete');

    } 

    $this->redirect_to(url_for(array(
      'route_name' => 'chained_modules_admin',
      'controller' => 'admin',
      'module' => $this->module,
      'maction' => 'request'
    )));
  }


}
?>