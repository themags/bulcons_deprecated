<?php 

class ProductsController extends ModulesController {

	public $module = 'products';
	public $models = 'ProductsModel ProductsCategory ProductsPackage';

	function __construct()
	{
		parent::__construct();
		$this->noac = true;
	}

	function index($params) {
		$obj = new $this->models[0];
		$params['sortable'] = true;
		$list = new ListHelper($params);
		if($this->admin_helper->can('edit')){
			$list->add_action('edit', url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'edit', 'id' => ':id')));
		}
		if($this->admin_helper->can('delete')){
			$list->add_action('delete', 'javascript:confirm_delete(:id);');
		}
		$list->add_action('view', url_for(['controller' => 'admin', 'maction' => 'list_package', 'module' => $this->module, 'id' => ':id']));
		$list->add_column('id');
		$list->add_column('title');
		$list->add_column('products_category_id');
	 
		$list->add_column('active');

		
		$categories = $this->ProductsCategory->find_all();
		foreach ($categories as $cat) {
			$this->categories[$cat->id] = $cat->title;
		}
		
		$list->add_filter('title','null','text');
		$list->add_filter('products_category_id', $this->categories, 'select');
		$list->add_filter('active', $this->localizer->get('yesno'), 'select');

		$items = $obj->find_all($list->to_sql(), 'ord ASC');    
		$list->hide_main_actions = false;
		$list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'index')),'label'=>'index'));
		$list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'categories')),'label'=>'categories'));
		
		$list->data($items);
		$this->render($list);
		$this->session->admin_return_to = $this->request->server('REQUEST_URI');
	}

	public function list_package($params) {
		$product_id = intval($params['id']);
		$this->product = $this->ProductsModel->find($product_id);
		//d($this->product);

		$list = new ListHelper(true);
		if($this->admin_helper->can('edit')){
			$list->add_action('edit', url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'edit_package', 'id' => ':id' , 'product_id'=> $this->product->id)));
		}

		$this->delete_action = 'delete_package';
		$list->add_action('delete', 'javascript:confirm_delete(:id);');

		$list->add_column('id');
		$list->add_column('weight');
		$list->add_column('product_title');

		$items = $this->ProductsPackage->find_all(array(
			'conditions' => 'product_id = ' . intval($this->product->id),
			'order' => 'ord'
		));    
		$list->hide_default_main_actions = true;

		$list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'add_package','product_id'=> $this->product->id)),'label'=>'add')); 
		//$list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'categories')),'label'=>'categories'));
		$list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'index')),'label'=>'products'));

		$list->data($items);            
		$this->render($list);
		$this->session->admin_return_to = $this->request->server('REQUEST_URI');
		// find All where product_id = product->id
	}

	public function add_package($params) {
		$product_id = intval($params['product_id']);
		$this->product = $this->ProductsModel->find($product_id);

		if($this->is_post()){
			$obj = $this->ProductsPackage;
			$obj->update_attributes(['product_id' => $this->product->id]);
			$response = new stdClass;
			$response->ok = 0;
			$response->errors = array();
			$response->redirect_to = '';

			if($obj->save($_POST)) {
				$this->handle_uploads($obj, $this->module);
				$this->log_action(array('id'=>$obj->id,'message' => (string)$obj));
				$this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'add');
				$response->ok = 1;
				$response->redirect_to = $this->session->admin_return_to;
			} else {
				$response->errors = $obj->errors;
			}
			$this->layout = 'json_save_callback';
			$this->content_for_layout =  json_encode($response);
		} else {
			$this->view = 'admin_form';
			$this->fields = $this->fields_helper->generate($this->module,$this->ProductsPackage);      
		}
		
	}

	public function edit_package($params) {
		$obj = $this->ProductsPackage;
		$object = $obj->find((int)$params['id']);
		if($object) {
			if($this->is_post()) {
				$response = new stdClass;
				$response->ok = 0;
				$response->errors = array();
				$response->redirect_to = '';

				if($object->save($_POST)) {
					$this->log_action(array('id'=>$object->id,'message' => (string)$object));
					$this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'edit');
					$response->ok = 1;
					$response->redirect_to = $this->session->admin_return_to;
				} else {
					$response->errors = $object->errors;
				}
				$this->layout = 'json_save_callback';
				$this->content_for_layout =  json_encode($response);
			}

			$this->form_object = $object;
			$this->view = 'admin_form';
			$this->fields = $this->fields_helper->generate($this->module,$object);  
		} else {
			$response->redirect_to = $this->session->admin_return_to;
		}
		
	}

	function delete_package($params){
		//use this for custom delete 
		$object = $this->ProductsPackage->find((int)$params['id']);

		if($object) {
			$object->delete();
			$this->log_action(array('id'=>$object->id,'message' => (string)$object));
			$this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'delete');

		} 

		$this->redirect_to(url_for(array(
			'route_name' => 'chained_modules_admin',
			'controller' => 'admin',
			'module' => $this->module,
			'maction' => 'list_package',
			'id' => $object->product_id
		)));
	}

	protected function save_order_list_package($params){
			$ids = explode(',', $_POST['ids']);
			foreach ($ids as $key => $id){
					Registry()->db->query('UPDATE '.$this->ProductsPackage->table_name.' SET ord='.(int)$key.' WHERE id='.$id.'');
			}
			$this->log_action();
			echo $this->localizer->get_label('KEYWORDS', 'order_saved');
	}

	function getList_products_category_id($v){
			return $this->categories[$v];
	}

	function getList_is_viewed($v){
		 return $this->localizer->get('yesno',(int)$v);
	}
	public function getList_product_title($value, $object) {
		if( $object instanceof ProductsPackage && $object->product instanceof ProductsModel ) {
			return $object->product->title;
		}
		elseif( $object instanceof ProductsModel ) {
			return $object->title;
		}

		return '-'; // just in case
	}
	/* Categories */
	
	function categories($params) {
		$list = new ListHelper(true);
		if($this->admin_helper->can('edit')){
			$list->add_action('edit', url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'edit_category', 'id' => ':id')));
		}
		if($this->admin_helper->can('delete_category')){
			$list->add_action('delete', url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'delete_category', 'id' => ':id')));
		}
		$list->add_column('id');
		$list->add_column('title');

		$items = $this->ProductsCategory->find_all($list->to_sql(), 'ord ASC');    
		$list->hide_default_main_actions = true;
		$list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'add_category')),'label'=>'add')); 
		$list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'categories')),'label'=>'categories'));
		$list->add_main_action(array('link'=>url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'index')),'label'=>'products'));

		$list->data($items);            
		$this->render($list);
		$this->session->admin_return_to = $this->request->server('REQUEST_URI');
	}

	function add_category($params){ 
		if($this->is_post()){
			$obj = $this->ProductsCategory;
			$response = new stdClass;
			$response->ok = 0;
			$response->errors = array();
			$response->redirect_to = '';

			if($obj->save($_POST)) {
				$this->handle_uploads($obj, $this->module);
				$this->log_action(array('id'=>$obj->id,'message' => (string)$obj));
				$this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'add');
				$response->ok = 1;
				$response->redirect_to = $this->session->admin_return_to;
			} else {
				$response->errors = $obj->errors;
			}
			$this->layout = 'json_save_callback';
			$this->content_for_layout =  json_encode($response);
		} else {
			$this->view = 'admin_form';
			$this->fields = $this->fields_helper->generate($this->module,$this->ProductsCategory);      
		}
	}

	function edit_category($params){
		$obj = $this->ProductsCategory;
		$object = $obj->find((int)$params['id']);
		if($object) {
			if($this->is_post()) {
				$response = new stdClass;
				$response->ok = 0;
				$response->errors = array();
				$response->redirect_to = '';

				if($object->save($_POST)) {
					$this->log_action(array('id'=>$object->id,'message' => (string)$object));
					$this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'edit');
					$response->ok = 1;
					$response->redirect_to = $this->session->admin_return_to;
				} else {
					$response->errors = $object->errors;
				}
				$this->layout = 'json_save_callback';
				$this->content_for_layout =  json_encode($response);
			}

			$this->form_object = $object;
			$this->view = 'admin_form';
			$this->fields = $this->fields_helper->generate($this->module,$object);  
		} else {
			$response->redirect_to = $this->session->admin_return_to;
		}
	}

	function delete_category($params){
		$object = $this->ProductsCategory->find((int)$params['id']);

		if($object) {
			$object->delete();
			$this->log_action(array('id'=>$object->id,'message' => (string)$object));
			$this->flash_message_helper[] = $this->localizer->get_label('FLASH_MESSAGES', 'delete');

		} 
		$this->redirect_to($this->session->admin_return_to);
	}

	protected function save_order_categories($params){
			$ids = explode(',', $_POST['ids']);
			foreach ($ids as $key => $id){
					Registry()->db->query('UPDATE '.$this->ProductsCategory->table_name.' SET ord='.(int)$key.' WHERE id='.$id.'');
			}
			$this->log_action();
			echo $this->localizer->get_label('KEYWORDS', 'order_saved');
	}

}
?>