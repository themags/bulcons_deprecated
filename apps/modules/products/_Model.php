<?php

    class ProductsModel extends Modules {

        public $module = 'products';
        protected $is_i18n = true;
        protected $has_mirror = true;
        public
		$table_name = 'products',
		$preserve_index = true;

        public $image_extensions = array(
                'image/jpeg'    => 'jpg',
                'image/jpg'     => 'jpg',
                'image/pjpeg'   => 'jpg',
                'image/gif'     => 'gif',
                'image/png'     => 'png'
        );

        protected $has_one = array(
            'picture' => array(
                'association_foreign_key'=>'id', 
                'foreign_key' => 'module_id', 
                'is_required' => false,
                'class_name'=>'image', 
                'join_table' => 'images', 
                'conditions' => "module = 'products' and keyname='picture'"
            ),
            'thumb' => array(
                'association_foreign_key'=>'id', 
                'foreign_key' => 'module_id', 
                'is_required' => false,
                'class_name'=>'image', 
                'join_table' => 'images', 
                'conditions' => "module = 'products' and keyname='thumb'"
            )
        );

        

        /*protected $has_many = array(
                'images' => array(
                    'association_foreign_key'=>'id', 
                    'foreign_key' => 'module_id', 
                    'is_required' => false,
                    'class_name'=>'image', 
                    'join_table' => 'images', 
                    'conditions' => "module = 'products' and keyname='images'")
         );*/

        protected $belongs_to = array(
            'products_category' => array(
                'association_foreign_key'=>'id', 
                'foreign_key' => 'products_category_id', 
                'class_name'=>'ProductsCategory', 
                'join_table' => 'products_category', 
                'autocomplete_column'=>'title')

        );

        protected $has_and_belongs_to_many = array(
                'recipes' => array(
                    'association_foreign_key' => 'recipe_id', 
                    'foreign_key' => 'product_id',
                    'class_name' => 'RecipesModel', 
                    'join_table' => 'recipes_products'
                ),
                'news' => array(
                    'association_foreign_key' => 'news_id', 
                    'foreign_key' => 'product_id',
                    'class_name' => 'NewsModel', 
                    'join_table' => 'products_news'
                )
         );

        function find_product_packages_by_city_and_country($city_name, $country_name) {
            $data = array();

            if($result = self::$db->select(
                // from
                "products"
                . " INNER JOIN products_i18n ON products_i18n.i18n_foreign_key = products.id AND products_i18n.i18n_locale = '" . $this->get_locale() . "'"
                . " INNER JOIN products_packages ON products_packages.product_id = products.id"
                . " INNER JOIN products_packages_i18n ON products_packages_i18n.i18n_foreign_key = products_packages.id AND products_packages_i18n.i18n_locale = products_i18n.i18n_locale"
                . " INNER JOIN products_shops ON products_shops.product_package_id = products_packages.external_id" 
                . " INNER JOIN shops ON shops.external_id = products_shops.shop_id"
                . " INNER JOIN shops_i18n ON shops_i18n.i18n_foreign_key = shops.id AND shops_i18n.i18n_locale = products_packages_i18n.i18n_locale"
                // select
                , "products_packages.external_id, CONCAT(products_i18n.title, ' ',  products_packages_i18n.weight) AS product_package"
                // where
                , sprintf(
                    "products_i18n.active = 1 AND LOWER(shops_i18n.city) = LOWER('%s') AND LOWER(shops_i18n.country) = LOWER('%s')", 
                    Registry()->db->escape($city_name),
                    Registry()->db->escape($country_name)
                )
            )) {
                if($result->num_rows() > 0) {
                    foreach ($result as $row) {
                        $data[$row->external_id] = $row->product_package;
                    }
                }
            }

            return $data;
        }
    }

    class ProductsCategory extends Modules {
        public $module = 'products';
        protected $is_i18n = true;
        protected  $has_mirror = true;
        public $table_name = 'products_categories';

        public $image_extensions = array(
                'image/jpeg'    => 'jpg',
                'image/jpg'     => 'jpg',
                'image/pjpeg'   => 'jpg',
                'image/gif'     => 'gif',
                'image/png'     => 'png'
        );
        
        protected $has_one = array(
            'image' => array(
                'association_foreign_key'=>'id', 
                'foreign_key' => 'module_id', 
                'is_required' => false,
                'class_name'=>'image', 
                'join_table' => 'images', 
                'conditions' => "module = 'products' and keyname='image'"
            )
        );
    }

    class ProductsPackage extends Modules {
        public $module = 'products';
        protected $is_i18n = true;
        protected  $has_mirror = true;
        public $table_name = 'products_packages';

        public $image_extensions = array(
                'image/jpeg'    => 'jpg',
                'image/jpg'     => 'jpg',
                'image/pjpeg'   => 'jpg',
                'image/gif'     => 'gif',
                'image/png'     => 'png'
        );
        
        protected $has_one = array(
            'package_image' => array(
                'association_foreign_key'=>'id', 
                'foreign_key' => 'module_id', 
                'is_required' => false,
                'class_name'=>'image', 
                'join_table' => 'images', 
                'conditions' => "module = 'products' and keyname='package_image'"
            )
        );
        protected $belongs_to = array(
            'product' => array(
                'association_foreign_key' => 'id', 
                'foreign_key' => 'product_id', 
                'class_name' => 'ProductsModel'
            )
        );
    }

    class ProductsRatings extends Modules {
        public $module = 'products';
        protected $is_i18n = false;
        protected  $has_mirror = false;
        public $table_name = 'products_ratings';

    }

?>