<?php
	
	$page = new Page;
	$obj = $page->find_by_page_type('products');

	$product_fake = new ProductsModel;
	$product_category = new ProductsCategory;
	$products = $product_fake->find_all('active=1');
	foreach($products as $a)
	{
		Router()->add(
			'products_inner',
			$obj->slug.'/'.$a->products_category->slug.'/'.$a->slug,
			array(
				'app'=>'products',
				'controller' => 'products',
				'action'=>'view_product',
				'slug' => $a->slug,
				'page_id'=>$obj->id,
				'id' => $a->products_category->id,
				'product_id' => $a->id,
				'page_slug' => $obj->slug
			)
		);
	}

	$all = $product_category->find_all(null,'ord ASC');
	foreach($all as $a)
	{
		Router()->add(
			'products_category', 
			$obj->slug.'/'.$a->slug, 
			array(
				'controller'=>'products',
				'app'=>'products', 
				'action'=>'view_category', 
				'id' => $a->id, 
				'page_id'=>$obj->id, 
				'slug' => $a->slug
				)
			);
	}

		Router()->add(
		'products_vote', 
		'/products/xhr/vote', 
		array(
			'controller' => 'products', 
			'app' => 'products', 
			'action' => 'xhr', 
			'method' => 'vote',
			)
		);
?>