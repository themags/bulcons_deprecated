<?php


class ProductsController extends ApplicationController {

    public $models = 'ProductsModel Page';
    protected $layout = 'index';

    function view_category($params) {   
    	//die('here');
       	$product_fake = new ProductsModel;
		$product_category = new ProductsCategory;
		$this->product_categories = $product_category->find_all(null,'ord ASC');
		$this->product = $product_category->find((int)$params['id']);
		//d('view_category');
		$this->obj = $this->Page->find((int)$params['page_id']);
		$this->faqs_page = $this->Page->find(15);

        /*$this->recipes_list = $fake->find_all_by_recipes_category_id_and_active((int)$params['id'] , 1, array('limit' => 4, 'order' => 'ord ASC'));
        $this->paginator = new Paginator(current($this->recipes_list)->pages, $_GET['page']);*/

        $this->products_list = $product_fake->find_all_by_products_category_id_and_active((int)$params['id'] , 1, array('limit' => 8, 'order' => 'ord ASC'));
        $this->paginator = new Paginator(current($this->products_list)->pages, $_GET['page']);

        $this->page_mirror = [];
        $page_slugs = $this->Page->find_by_sql("SELECT * FROM pages_i18n WHERE visibility = 2 AND i18n_foreign_key = " . $this->obj->id);
        $categories_slugs = $product_category->find_by_sql("SELECT * FROM products_categories_i18n WHERE i18n_foreign_key = " . $this->product->id);
        
        foreach ($page_slugs as $key => $value) {
           $this->page_mirror[$value->get_locale()] = $value->slug;
        }

        foreach ($categories_slugs as $key => $value) {
            if($this->page_mirror[$value->get_locale()]) {
                $this->page_mirror[$value->get_locale()] .= '/' . $value->slug;
            }
        }
    }
    
    function view_product($params) {
        //d($params);
    	//d('view_product');
        $product_fake = new ProductsModel;
        $product_category = new ProductsCategory;
        $votes = new ProductsRatings;
        $this->avg_vote = $votes->avg_all('rating', 'product_id = "'.$params['product_id'].'"')*20;
        $this->product = $product_category->find((int)$params['id']);
        $products = $product_fake->find_all('active=1');
        $package_model = new ProductsPackage;
        $this->package_items = $package_model->find_all('product_id = '. (int)$params['product_id'], 'ord ASC');
        $this->parent_page_slug = $params['page_slug'];
        $this->recipe_page_slug = $this->Page->find(16)->slug;
        $this->product_item = $product_fake->find_by_slug_and_active($params['slug'] , 1);
        $this->obj = $this->Page->find((int)$params['page_id']);
        $this->url = Config()->BASE_URL.Registry()->request->request_uri;
       

        $already_voted_ids = array();
        if(isset($_COOKIE[Config()->VOTE_COOKIE_NAME])){
            $already_voted_ids = explode(',', $_COOKIE[Config()->VOTE_COOKIE_NAME]);
        }

        if (in_array($params['product_id'], $already_voted_ids)) {
            $this->already_voted = 1;
        }


        $this->title = $this->product_item->title;

        $this->og_info['type'] = 'article';
        $this->og_info['title'] = $this->product_item->title;
        $this->og_info['description'] =  $this->product_item->summary;
        $this->og_info['url'] = $this->url;
        $this->og_info['image'] = Config()->BASE_URL.Config()->PUBLIC_URL.'files/'.$this->product_item->module.'/'.$this->product_item->id.'/'.$this->product_item->picture->keyname.'/'.$this->product_item->picture->filename;
    }

    function vote($params) {
        //d($params['vote']);
        $response = new stdClass;
        $response = array();
        $products = new ProductsModel;
        $vote_obj = new ProductsRatings;

        $already_voted_ids = array();

        if (isset($_COOKIE[Config()->VOTE_COOKIE_NAME]) && preg_match('/^\d+(,\d+)*$/', $_COOKIE[Config()->VOTE_COOKIE_NAME])) {
            $already_voted_ids = explode(',', $_COOKIE[Config()->VOTE_COOKIE_NAME]);
        }

        if (in_array($params['product_id'], $already_voted_ids)) {
            $response['succses'] = false;
            $response['vote_message'] = Registry()->localizer->get('vote_message');
        }else{
            $already_voted_ids[] = $vote_obj->product_id = $params['product_id'];
            $vote_obj->rating = $params['vote'];
            $vote_obj->save();
            $this->avg_vote = $vote_obj->avg_all('rating', 'product_id = "'.$params['product_id'].'"');
            $response['succses'] = true;
            $response['avg_vote'] = $this->avg_vote;

            setcookie(Config()->VOTE_COOKIE_NAME, implode(',', $already_voted_ids), time() + 60 * 60 * 24 * 365, Config()->COOKIE_PATH);
        }
        $this->content_for_layout = json_encode($response);
    }

}    
?>