<?php

class NewsWidget extends BlockHelper {
	private
		$_breaking_news_id = 75;

	protected
		$_pagination_url_params = array('module' => 'news', 'slug_id' => ''),
		$_breaking_news_page_slug = null;

	function __construct() {
		parent::__construct();
		$this->_breaking_news_page_slug = (new Page())->find_by_id($this->_breaking_news_id, array('conditions' => 'visibility=2'))->slug;
	}

	function index($block) {
		$model = new NewsModel;
		$params = unserialize($block->params);

		$order = in_array($params['order'], array('published_datetime ASC', 'published_datetime DESC')) ? $params['order'] : 'published_datetime DESC';
		$limit = (int)$params['limit'] ? (int)$params['limit'] : 9;
		$limit = 9;

		$items = $model->find_all('active=1 AND published_datetime <= NOW()', $order, $limit);
		$paginator = new Paginator(current($items)->pages, $_GET['page']);

		$options = array(
			'partial' => __FUNCTION__ ,
			'items' => $items,
			'paginator' => $paginator,
			'params' => $params,
			'page_id' => $block->page_id,
			'page' => $block->page,
			'block_id' => $block->id,
			'cache_hash' => $block->cache_hash,
			'smarty_cache_id' => __FUNCTION__ . '_news_index_' . $block->id . '_' . Registry()->locale,
		);

		return $this->render_block($options);
	}

	function last_news($block) {
		$model = new NewsModel;
		$params = unserialize($block->params);
		$params['breaking_news_slug'] = $this->_breaking_news_page_slug;

		$order = in_array($params['order'], array('published_datetime ASC', 'published_datetime DESC')) ? $params['order'] : 'published_datetime DESC';
		$limit = (int)$params['limit'] ? (int)$params['limit'] : 2;

		$items = $model->find_all('active=1 AND published_datetime <= NOW()', $order, $limit);

		$p = new Page;

		$options = array(
			'partial' => __FUNCTION__ ,
			'items' => $items,
			'news_page' => $p->find_by_page_type('news'),
			'params' => $params,
			'page_id' => $block->page_id,
			'block_id' => $block->id,
			'cache_hash' => $block->cache_hash,
			'smarty_cache_id' => __FUNCTION__ . '_news_' . $block->id . '_' . Registry()->locale,
		);

		return $this->render_block($options);
	}

	function view($params) {
		header('Content-type: application/json');

		$object = null;
		$model = new NewsModel;

		if(!empty($params['slug_id']) && stristr($params['slug_id'], '+')) {
			list($slug, $id) = explode(' ', urldecode($params['slug_id']));
			$object = $model->find_by_id((int)$id, array('conditions' => "active=1 AND slug='" . Registry()->db->escape($slug) . "'"));
		}

		return $this->render_block(array(
			'params' => $params,
			'partial' => 'view',
			'object' => $object,
			'smarty_cache_id' => __FUNCTION__ . '_news_view_' . $params['slug_id'] . '_' . $_REQUEST['page'] . '_' . Registry()->locale,
		));
	}
}

?>