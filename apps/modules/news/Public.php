<?php
	class NewsController extends ApplicationController {

		public $models = 'NewsModel Page';
		protected $layout = 'index';

		function view($params) {
			$model = new NewsModel;
			$this->obj = $model->find_by_slug_and_active($params['slug'],1);
			$this->slug = $params['main_page_slug'];
			if(!$this->obj) {
				 $this->redirect_to('/');
			}
		}
	}
?>