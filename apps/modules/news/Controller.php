<?php

class NewsController extends ModulesController {
	public
		$module = 'news',
		$models = 'NewsModel';

	function index($params) {
		$obj = new $this->models[0];
		$list = new ListHelper($params);

		if($this->admin_helper->can('edit')) {
			$list->add_action('edit', url_for(array('controller' => 'admin', 'module' => $this->module, 'maction' => 'edit', 'id' => ':id')));
		}

		if($this->admin_helper->can('delete')) {
			$list->add_action('delete', 'javascript:confirm_delete(:id);');
		}

		$list->add_column('id');
		$list->add_column('title');
		$list->add_column('active');

		$list->add_filter('title', 'null', 'text');
		$list->add_filter('published_datetime_from', 'null', 'datetime');
		$list->add_filter('published_datetime_to', 'null', 'datetime');
		$list->add_filter('active', $this->localizer->get('yesno'), 'select');


		$items = $obj->find_all($list->to_sql(), 'published_datetime DESC', $obj->rows_per_page_default);
		$list->data($items);
		$this->render($list);

		$this->session->admin_return_to = $this->request->server('REQUEST_URI');
	}

	function getFilter_published_datetime_from($published_datetime) {
		return "published_datetime >='" . $published_datetime . "'";
	}

	function getFilter_published_datetime_to($published_datetime) {
		return "published_datetime <='" . $published_datetime . "'";
	}
}

?>