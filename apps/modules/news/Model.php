<?php

class NewsModel extends Modules {
	public
		$module = 'news',
		$table_name = 'news',
		$image_extensions = array(
			'image/jpeg' => 'jpg',
			'image/jpg' => 'jpg',
			'image/pjpeg' => 'jpg',
			'image/gif' => 'gif',
			'image/png' => 'png'
		);

	protected
		$is_i18n = true,
		$has_mirror = true,
		$has_one = array(
			'photo' => array(
				'association_foreign_key' => 'id', 
				'foreign_key' => 'module_id', 
				'class_name' => 'image', 
				'join_table' => 'images', 
				'is_required' => true, 
				'conditions' => "module = 'news' and keyname='photo'"
			),
			'image' => array(
				'association_foreign_key' => 'id', 
				'foreign_key' => 'module_id', 
				'class_name' => 'image', 
				'join_table' => 'images', 
				'is_required' => true, 
				'conditions' => "module = 'news' and keyname='image'"
			)
		),

		/*$has_many = array(
			'images' => array(
				'association_foreign_key' => 'id', 
				'foreign_key' => 'module_id', 
				'class_name' => 'image', 
				'join_table' => 'news', 
				'is_required' => false, 
				'conditions' => "module = 'news' and keyname = 'images'"
			),
		),*/

		$has_and_belongs_to_many = array(
                'products' => array(
                    'association_foreign_key' => 'product_id', 
                    'foreign_key' => 'news_id',
                    'class_name' => 'ProductsModel', 
                    'join_table' => 'products_news'
            ),
             	'pages' => array(
             		'foreign_key' => 'news_id', 
             		'association_foreign_key' => 'page_id', 
             		'class_name' => 'Page', 
             		'join_table' => 'news_pages', 
             		'autocomplete_column' => 'title',  
             		'order' => 'lft ASC'
             	),   
        );

			
	public
		$admin_fields_order = array(
			'title',
			'slug',
			'published_datetime',
			'summary',
			'content',
			'photo',
			'active'
		);
	function get_products_for_admin() {
        $data = array();
        if($result = self::$db->select("products"
            . " INNER JOIN products_i18n ON products_i18n.i18n_foreign_key = products.id AND products_i18n.i18n_locale = '" . $this->get_locale() . "'",
            'id, title'
        )) {
            foreach($result as $row) {
                $data[] = $row->fetch();
            }
        }

        return ars($data)->toSmartySelect();
    }
}

?>