<?php 

class FaqsController extends ModulesController {

  public $module = 'faqs';
  public $models = 'FaqsModel';

  function index($params) {

    $obj = new $this->models[0];
    $params['sortable'] = true;
    $list = new ListHelper($params);
    $list->add_action('edit', url_for(array('controller'=>'admin','module' => $this->module,'maction'=>'edit', 'id' => ':id')));
    $list->add_action('delete', 'javascript:confirm_delete(:id);');
    $list->add_column('id');
    $list->add_column('question_title');
    $list->add_column('answer');
    $list->add_column('active');

    
    $list->add_filter('question_title','null','text');
    $list->add_filter('active', $this->localizer->get('yesno'), 'select');


    $items = $obj->find_all($list->to_sql(), 'ord ASC');    
    $list->data($items);
    $this->render($list);
    $this->session->admin_return_to = $this->request->server('REQUEST_URI');
  }
}
?>