<?php 

class ImportsController extends ModulesController {

		public $module = 'imports';

		function index($params) {
			$this->view = 'import_stores';
			$this->error_message = 0;
			$this->success_message = 0;
			$this->invalid_format  = 0;
			$csv_mimetypes = array(
				'text/csv',
				'text/plain',
				'application/csv',
				'text/comma-separated-values',
				'application/excel',
				'application/vnd.ms-excel',
				'application/vnd.msexcel',
				'text/anytext',
				'application/octet-stream',
				'application/txt',
			);

			$db = SqlFactory::factory(Config()->DSN);

			if ($this->is_post()) {

				if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {

					if(in_array($_FILES['userfile']['type'], $csv_mimetypes) && ($info = pathinfo($_FILES['userfile']['name'])) && ($info['extension'] == 'csv')) {

						$csv_file = $_FILES['userfile']['tmp_name'];
						$arrResult  = array();
						$handle     = fopen($csv_file, "r");

						$document_title = fgetcsv($handle, 1000, ",");
						$document_header = fgetcsv($handle, 1000, ',');

						if(empty($handle) === false) {
						
						    while(($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
						    	$data = array_map('trim', $data);
						    	$where = sprintf("external_id = '%s'", $data[0]);
							    $result = $db->select('shops','external_id',$where);

						    	if($result->external_id) {

						    		$attributes = array(
							    		'updated_at' => date('Y-m-d H:i:s'),
							    		'number_of_partner' => $data[1],
							    		'website' => $data[13],
							    		'email' => $data[11],
							    		'is_online_shop' => 0,
							    		'zip_code' => $data[4],
							    		'lat' => $data[7],
							    		'lon' => $data[8],
							    	);
						    		
						    		$db->update('shops', $attributes, $where);
						    		$id = $db->select('shops','id', $where);

						    		//foreach (Config()->LOCALE_SHORTCUTS as $short_locale => $full_locale) {
						    			$i18n_where = sprintf("i18n_foreign_key = '%d' AND i18n_locale='bg-BG'", $id->id);
							    		$i18n_attributes = array(
									    	'name' => $data[2],
									    	'city' => $data[5],
									    	'street1' => $data[9],
									    	'street2'=> $data[10],
									    	'working_time_from_monday_to_friday' => $data[14],
									    	'working_time_saturday' => $data[15],
									    	'working_time_sunday' => $data[16],
									    	'country' => $data[6],
									    	'primary_contact'=> $data[12],
								    	);
							    		$db->update('shops_i18n', $i18n_attributes, $i18n_where);
							    	//}

							    }else {

						    		$attributes = array(
							    		'created_at' => date('Y-m-d H:i:s'),
							    		'external_id' => $data[0],
							    		'number_of_partner' => $data[1],
							    		'website' => $data[13],
							    		'email' => $data[11],
							    		'is_online_shop' => 0,
							    		'zip_code' => $data[4],
							    		'lat' => $data[7],
							    		'lon' => $data[8],
							    	);

							    	$id = $db->insert('shops', $attributes);

							    	foreach (Config()->LOCALE_SHORTCUTS as $short_locale => $full_locale) {
							    		$i18n_attributes = array(
							    			'i18n_foreign_key' => $id,
									    	'i18n_locale' => $full_locale,
									    	'name' => $data[2],
									    	'city' => $data[5],
									    	'street1' => $data[9],
									    	'street2'=> $data[10],
									    	'working_time_from_monday_to_friday' => $data[14],
									    	'working_time_saturday' => $data[15],
									    	'working_time_sunday' => $data[16],
									    	'country' => $data[6],
									    	'primary_contact'=> $data[12],
									    );
										$db->insert('shops_i18n',$i18n_attributes);
									}	
						    	}
							}
						}

						$this->success_message = 1;
					}else {
						$this->invalid_format = 1;
					}
					fclose($handle);
					
				}else {
					$this->error_message = 1;
				}
			}
		}

		function import_products_to_accounts($params) {
			$this->error_message = 0;
			$this->success_message = 0;
			$this->invalid_format = 0;
			$csv_mimetypes = array(
				'text/csv',
				'text/plain',
				'application/csv',
				'text/comma-separated-values',
				'application/excel',
				'application/vnd.ms-excel',
				'application/vnd.msexcel',
				'text/anytext',
				'application/octet-stream',
				'application/txt',
			);
			$db = SqlFactory::factory(Config()->DSN);

			if($this->is_post()) {

				if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {

					if(in_array($_FILES['userfile']['type'], $csv_mimetypes) && ($info = pathinfo($_FILES['userfile']['name'])) && ($info['extension'] == 'csv')) {
						$csv_file = $_FILES['userfile']['tmp_name'];
						$handle     = fopen($csv_file, "r");

						$document_title = fgetcsv($handle, 1000, ",");
						$document_header = fgetcsv($handle, 1000, ",");
						
						if(empty($handle) === false) {
						
						    while(($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
						 		$where = sprintf("product_package_id = '%s'AND shop_id = '%s'", $data[0], $data[3]);
							    $result = $db->select('products_shops','*',$where);

							    if($result->product_package_id && $result->shop_id){
						    		$delete  = $db->delete('products_shops', $where);
						    	}

						 		$attributes = array(
						 			'product_package_id' => $data[0],
						 			'shop_id' =>$data[3],
						 		);

						 		$insert = $db->insert('products_shops', $attributes);
							}	
						}
						$this->success_message = 1;
						fclose($handle);
					}else{
						$this->invalid_format = 1;
					}

				} else {
					$this->error_message = 1;
				}
			}
		}
	}
?>