<?php

class BulconsHelper extends BaseHelper {
	protected $_smarty_register = array(
		'functions' => array('build_order_url'),
		'modifiers' => array(),
	);

	function build_order_url_smarty($params, Smarty_Internal_Template $smarty) {
		if(!empty($params['order_by'])) {
			$url = Registry()->request->get_protocol() . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$url = explode('?', $url);
			$params['get']['order_by'] = $params['order_by'];
			return $url[0] . '?' . http_build_query($params['get']);
		}
	}
}

?>