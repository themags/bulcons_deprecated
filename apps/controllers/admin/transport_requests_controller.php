<?php

class TransportRequestsController extends AdminController {
    public
        $models = 'TransportRequest';

    function index($params) {
        $list = new ListHelper($params);
        $list->add_column('id');
        $list->add_column('izh_num');
        $list->add_column('date');
        $list->add_column('is_viewed');
       
        if($this->admin_helper->can('view')){
            $list->add_action('view', url_for(array('controller' => $this->get_controller_name(), 'action' => 'view', 'id' => ':id')));
        }
        /*if($this->admin_helper->can('delete')){
            $list->add_action('delete', 'javascript:confirm_delete(:id);');
        }*/

        $items_collections = $this->TransportRequest->find_all(null, $this->TransportRequest->get_order(), 30);
        $list->data($items_collections);
        $this->render($list);
        $this->session->admin_return_to = $this->request->server('REQUEST_URI');
    }

    function getList_is_viewed ($v) {
    	return $this->localizer->get('yesno', (int) $v);
    }

    function view($params) {
    	$this->object = $this->TransportRequest->find(intval($params['id']));

    	if( ! $this->object instanceof TransportRequest ) {
    		$this->error404();
    	}

    	if( $this->object->is_viewed == 0 ) {
	    	$this->object->is_viewed = 1;
	    	$this->object->save();
	    }
    }
}

?>