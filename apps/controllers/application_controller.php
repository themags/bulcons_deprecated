<?php

class ApplicationController extends ActionController {
	private $_newsletter_autologin_secret = null;
	protected $layout = array(
		'404' => array(':only' => array('error404')),
		'xhr' => array(':only' => array('xhr')),
		'index' => array(':except' => array('xhr'))
	);

	function __construct() {
		parent::__construct();

		$this->add_helper('thumbnail');
		if(Registry()->is_public) {
			//$this->add_before_filter('mobile_detection');
			$this->add_after_filter('_setMeta');
			$this->add_after_filter('_generate_menus');
			$this->add_after_filter('_reset_cookie_for_last_accessed_page');
			$this->add_helper('bulcons');
			$this->add_before_filter('cookies_enabled');
			$this->add_before_filter('_loadNeededVariables');
		} else {
			//$this->add_before_filter('_prepareNewsletterForCMS');
		}
	}

	protected function _loadNeededVariables() {
		$this->cookie_slug = (new Page)->find_by_id(25)->slug;
	}

	protected function cookies_enabled() {
		if ($_COOKIE['cb-enabled'] == 'accepted') {
			return $this->cookies_enabled = true;
		}

		$this->cookies_enabled = false;
	}

	protected function _geolocate() {
		if(!$this->session->get('geolocated')) {
			require(Config()->LIB_PATH . "/geoiploc/geoiploc.php");
			$request = Registry()->request;
			$ip = $_SERVER['REMOTE_ADDR'];

			// if country code has been geo located and exists among declared languages and is different then currently set, set cookie to remember it and redirect
			if(($country_code = strtolower(getCountryFromIP($ip,'code')))
				&& $country_code != array_search(Registry()->locale, Config()->LOCALE_SHORTCUTS)
				&& array_key_exists($country_code, Config()->LOCALE_SHORTCUTS)
			) {
				// add page slug if found, in case someone has changed main page (home title)
				$slug = '';
				if($rows = Registry()->db->select('pages_i18n', 'i18n_locale, slug',array('visibility' => 2, 'i18n_foreign_key' => 1))) {
					foreach($rows as $row) {
						$slug = $row->i18n_locale == Config()->LOCALE_SHORTCUTS[$country_code] ? $row->slug : '';
					}
				}

				$this->session->set('geolocated', Config()->LOCALE_SHORTCUTS[$country_code]);
				Registry()->session->locale = Registry()->locale = Config()->LOCALE_SHORTCUTS[$country_code];
				header('Location: ' . $request->get_protocol() . $request->server('SERVER_NAME') . Config()->COOKIE_PATH . $country_code . $slug);
			}
		}
	}

	protected function _generate_menus() {
		if(!$this->session->get('geolocated')) {
			$this->session->set('geolocated', Registry()->locale);
		}

		$page = new Page;
		$this->main_menu_l = $page->find(1)->get_children(" AND visibility=2 AND exclude_from_menu = 0 AND menu_position = 0");
		$this->main_menu_r = $page->find(1)->get_children(" AND visibility=2 AND exclude_from_menu = 0 AND menu_position = 1");
		$this->footer_menu = $page->find(1)->get_children(" AND visibility=2 AND in_footer = 1");
		$model = new ContactsModel();
		$this->list = $model->find('active=1');
		$this->contact_page_slug = (new Page())->getSlugForContacts();
	}

	protected function mobile_detection() {
		if(!isset(Registry()->session->isMobile)) {
			require_once Config()->LIB_PATH . '/Mobile_Detect.php';
			$detect = new Mobile_Detect();
			Registry()->session->isMobile = $detect->isMobile();
		}
	}

	function index($params) {}

	function error404() {
		$this->layout = 404;
		$this->_view = '__no_view';
		$this->_is404 = true;
		//$this->service = $this->Page->find(111);
		//$this->services = $this->service->get_children(' and page_type = "services" and visibility = 2');

		//http_response_code(404);
		header("HTTP/1.0 404 Not Found");
	}

	function xhr($params) {
		if ($this->is_xhr()) {
			$this->layout = 'xhr';

			if(!empty($_POST)) {
				$params = $params + $_POST;
			}

			$method = $params['method'];
			unset($params['method']);

			if(Registry()->is_public && !empty($params['module'])) {
				if(!$module_key = array_search($params['module'], (array)Config()->MODULES)) {
					goto response404;
				}

				if(!isset($params['class'])) {
					$module = Inflector::camelize($params['module']) . 'Widget';
					$widget = new $module;
				} else {
					if(!file_exists($module_file = Config()->MODULES_PATH . Config()->MODULES[$module_key] . DIRECTORY_SEPARATOR .'Widgets.php')) {
						goto response404;
					}

					include_once($module_file);

					$module = Inflector::camelize($params['class']);
					if(!$widget = new $module) {
						$module = Inflector::camelize($params['module']);
						$widget = new $module;
					}

					unset($params['class']);
				}

				unset($module, $params['module']);


				if(!method_exists($widget, $method)) {
					goto response404;
				}

				$this->content_for_layout = $widget->{$method}($params);
				empty($this->content_for_layout) && ($this->content_for_layout = ' ');
			} else {
				if(!method_exists($this, $method)) {
					goto response404;
				}

				$this->{$method}($params);
			}

			goto responseSuccess;


			response404: {
				//http_response_code(404);
				header("HTTP/1.0 404 Not Found");
				$this->content_for_layout = 'Method `' . $method . '` not found in: ' . get_class($this);
			}

			responseSuccess: {}
		} else {
			$this->error404();
		}
	}

	public function is_logged() {
		if(empty($this->session->user_data['name'])) {
			$this->redirect_to(url_for(array('controller' => 'profiles', 'action' => 'login')));
		}
	}

	public static function escapeData($data) {
		foreach($data as $key => $post) {
			if(is_array($post)) {
				foreach($post as $k => $p) {
					$data[$key][$k] = Registry()->db->escape($p);
				}
			} else {
				$data[$key] = Registry()->db->escape($post); //escapes all data
			}
		}

		return $data;
	}
	protected function _setMeta() {
		if($this->obj && $this->obj instanceof Page) {
			$this->meta_title = $this->obj->meta_title;
			$this->meta_keywords = $this->obj->meta_keywords;
			$this->meta_description = $this->obj->meta_description;
		}

		if(!$this->meta_title) {
			$this->meta_title = $this->localizer->get_label('META', 'title');
		}

		if(!$this->meta_keywords) {
			$this->meta_keywords = $this->localizer->get_label('META', 'keywords');
		}

		if(!$this->meta_description) {
			$this->meta_description = $this->localizer->get_label('META', 'description');
		}
	}

	function _reset_cookie_for_last_accessed_page() {
		$last_accessed_base_url_cookie = 'last_accessed_base_url';
		$url = url_for(array(
			'host' => $this->all_countries_and_languages['by_country_id'][$this->current_domain_information['id']],
			'lang' => ($this->_url_params['lang']) ? $this->_url_params['lang'] : 'en',
			'controller' => 'application',
			'action' => 'index',
		));

		$base_domain = new Country();
		$base_domain->load(1);
		setcookie($last_accessed_base_url_cookie , $url, $expire = strtotime(time("+720 days")),'/' , ".".$base_domain->domain);
	}
	/*
	 Handlers for automatic file and image uploads
	 */
	protected function upload_image($params) {
		if($params['model']){
			$model_name = Inflector::modulize($params['model']);
			$model = new $model_name;
		} else {
			$model = new $this->models[0];
		}

		if(isset($params['id']) && $params['id'] > 0) {
			$image = new Image();
		} else {
			$image = new TempImage();
			$tmp = true;
		}

		$module = $model->table_name ? $model->table_name : $this->module;

		$image->id = 'NULL';
		$image->has_many = $model->get_has_many();
		$image->has_one = $model->get_has_one();
		$image->module = $module;
		$image->module_id = (int)$params['id'];
		$image->keyname = $params['keyname'];
		$image->thumbs = $model->thumbs;
		$image->image_options = $model->image_options;
		$image->image_extensions = $model->image_extensions;
		$image->save();

		$response = new stdClass();
		$response->id = $image->id;
		$response->name = $image->filename;
		$response->img = $image->get_file_path($tmp) . $image->filename;
		$response->size = $image->pretty_size();
		$response->keyname = $image->keyname;

		if($image->errors) {
			$response->error = $image->errors;
		}

		$response->delete_url = url_for(array('action'=>'xhr', 'method'=>'delete_image', 'id'=>$image->id, 'temp' => $tmp));

		echo json_encode($response);
		exit;
	}

	protected function get_images($params) {
		$model = !empty($params['model']) ? $params['model'] : $this->models[0];
		$model = new $model;
		$module = $model->table_name ? $model->table_name : $this->module;

		if(isset($params['id']) && $params['id']) {
			$image = new Image();
			$images = $image->find_all_by_module_id_and_module_and_keyname((int)$params['id'], $module, $params['keyname'], 'ord ASC');
		} else {
			$image = new TempImage();
			$images = $image->find_all_by_admin_user_id_and_module_and_keyname(unserialize(Registry()->session->userinfo)->id, $module, $params['keyname'], 'ord ASC');
			$tmp = true;
		}

		$response = array();
		foreach($images as $image) {
			$response['images'][] = array(
				'id'		 => $image->id,
				'name'		 => $image->filename,
				'img'		 => $image->get_file_path($tmp) . $image->filename,
				'size'	 	 => $image->pretty_size(),
				'keyname'	 => $image->keyname,
				'delete_url' => url_for(array('action'=>'xhr', 'method'=>'delete_image', 'id'=>$image->id, 'temp' => $tmp))
			);
		}

		if(!empty($params['return'])) {
			return $response;
		} else {
			echo json_encode($response);
			exit;
		}
	}

	protected function delete_image($params) {
		$model = new $this->models[0];

		if($params['temp']){
			$image = new TempImage();
		} else {
			$image = new Image();
		}

		$object = $image->find((int)$params['id']);
		$object->thumbs = $model->thumbs;
		$object->delete();
		exit;
	}

	protected function sort_images($params) {
		if($params['temp']) {
			$image = new TempImage();
		} else {
			$image = new Image();
		}

		foreach ($params['f'] as $key => $id) {
			Registry()->db->query('UPDATE '.$image->table_name.' SET ord='.(int)$key.' WHERE id='.$id.'');
		}
		exit;
	}

	protected function upload_file($params){
		if($params['model']) {
			$model = new $params['model'];
		} else {
			$model = new $this->models[0];
		}

		if(isset($params['id']) && $params['id'] > 0) {
			if(isset($params['file_model']) && $params['file_model'] == 'FileExtra') {
				$file = new FileExtra();
				$file->caption = '';
			} else if (isset($params['file_model']) && $params['file_model'] == 'CatalogueFileExtra') {
			    $file = new CatalogueFileExtra();
			    $file->caption = '';
			} else {
				$file = new File();
			}
		} else {
			$file = new TempFile();
			$tmp = true;
		}

		$module = $model->table_name ? $model->table_name : $this->module;

		$file->has_many = $model->get_has_many();
		$file->has_one = $model->get_has_one();
		$file->module = $module;
		$file->module_id = (int)$params['id'];
		$file->keyname = $params['keyname'];
		$file->file_extensions = $model->file_extensions;
		$file->save();

		$response = new stdClass();
		$response->id = $file->id;
		$response->name = $file->filename;
		$response->file = $file->get_file_path($tmp) . $file->filename;
		$response->size = $file->pretty_size();
		$response->keyname = $file->keyname;

		if($file->errors) {
			$response->error = $file->errors;
		}

		$response->delete_url = url_for(array('action' => 'xhr', 'method' => 'delete_file', 'id' => $file->id, 'temp' => $tmp));

		echo json_encode($response);
		exit;
	}

	protected function get_files($params) {
		$model = !empty($params['model']) ? $params['model'] : $this->models[0];
		$model = new $model;
		$module = $model->module ?: $this->module;
		$module = $model->table_name ? $model->table_name : $this->module;

		if(isset($params['id']) && $params['id']) {
			if(isset($params['file_model']) && $params['file_model'] == 'FileExtra'){
				$handler = new FileExtra();
			} elseif (isset($params['file_model']) && $params['file_model'] == 'CatalogueFileExtra') {
			    $handler = new CatalogueFileExtra();
			} else {
				$handler = new File();
			}

			$files = $handler->find_all_by_module_id_and_keyname_and_module((int)$params['id'], $params['keyname'], $module, 'ord ASC');
		} else {
			$handler = new TempFile();
			$files = $handler->find_all_by_admin_user_id_and_module_and_keyname(unserialize(Registry()->session->userinfo)->id, $module, $params['keyname'], 'ord ASC');
			$tmp = true;
		}

		$response = array();
		foreach($files as $file) {
			$response['files'][] = array(
				'id'		 => $file->id,
				'name'		 => $file->filename,
				'file'		 => $file->get_file_path($tmp) . $file->filename,
				'size'	 	 => $file->pretty_size(),
				'keyname'	 => $file->keyname,
				'delete_url' => url_for(array('action'=>'xhr', 'method'=>'delete_file', 'id'=>$file->id, 'temp' => $tmp, 'file_model' => get_class($file)))
			);
		}

		if(!empty($params['return'])) {
			return $response;
		} else {
			echo json_encode($response);
			exit;
		}
	}

	protected function delete_file($params) {
		if($params['temp']) {
			$handler = new TempFile();
		} else {
			if(isset($params['file_model']) && $params['file_model'] =='FileExtra') {
				$handler = new FileExtra();
			} else if (isset($params['file_model']) &&  $params['file_model'] == 'CatalogueFileExtra') {
			    $handler = new CatalogueFileExtra();
			} else {
				$handler = new File();
			}
		}

		$file = $handler->find((int)$params['id']);
		$file->delete();
		exit;
	}

	protected function sort_files($params) {
		if($params['temp']) {
			$handler = new TempFile();
		} else {
			$handler = new File();
		}

		foreach ($params['f'] as $key => $id) {
			Registry()->db->query('UPDATE '.$handler->table_name.' SET ord='.(int)$key.' WHERE id='.$id.'');
		}
		exit;
	}

	protected function files_extra_attributes($params) {
		if(isset($params['id'])) {
			if($params['action_type'] == edit) {
				$file_obj = new $params['file_model'];
			} else {
				$file_obj = new TempFile();
			}
		}

		$file = $file_obj->find($params['id']);
		$file->caption = $params['caption'];
		$file->save();

		if($file->errors) {
			$response->error = $file->errors;
		} else {
			$response->success = true;
		}
		echo json_encode($response);
		exit;
	}

	function handle_uploads($obj, $module) {
		$has_many = $obj->get_has_many();
		$has_one = $obj->get_has_one();

		if($has_many) {
			foreach($has_many as $keyname => $conditions) {
				if(in_array($conditions['class_name'], array('image', 'file'))) {
					$handler_name = Inflector::camelize($conditions['class_name']);
					$handler = new $handler_name();
					$handler->copy_temp($keyname, $module, $obj);
				}
			}
		}

		if($has_one) {
			foreach($has_one as $keyname => $conditions) {
				if(in_array($conditions['class_name'], array('image', 'file'))) {
					$handler_name = Inflector::camelize($conditions['class_name']);
					$handler = new $handler_name();
					$handler->copy_temp($keyname, $module, $obj);
				}
			}
		}

	}
}

?>