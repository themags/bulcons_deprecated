<?php
	class SearchController extends ApplicationController
	{
		function index($params)
		{
			if ($q = $params['q']) {
				$this->items = Search::get_results($q);
			}
		}
	}
?>