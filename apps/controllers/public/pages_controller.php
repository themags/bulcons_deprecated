<?php

class PagesController extends ApplicationController {
	public
		$models = 'Page',
		$breadcrumbs = array(),
		$slug_suffix = '';

	public $homepages = array(
		'bg-BG' => 2,
		'en-US' => 2,
		'ru-RU' => 2,
	);

	protected
		$layout = 'index';

	function __construct() {
		parent::__construct();
		if('index' == Registry()->request->get_action()) {
			$this->add_before_filter('_geolocate');
		}
	}

	function index($params) {
		$this->main_services = array();
		if($main_page = $this->Page->find(1)) {
			if(($this->main_services = $main_page->get_children()) && $this->default_thumb_sizes) {
				foreach($this->main_services as $sk => $service) {
					if(!empty($service->image)) {
						$thumb_details = pathinfo($service->image);
						foreach ($this->default_thumb_sizes as $thumb) {
							$prefix = 'thumb_' . $thumb . '_';

							// create thumb if not exists
							!file_exists(Config()->ROOT_PATH . ltrim($thumb_details['dirname'], '/') . DS . $prefix .  $thumb_details['basename']) && Files::create_thumbnail(Config()->ROOT_PATH . ltrim($thumb_details['dirname'], '/') . DS, $thumb_details['basename'], explode('x', $thumb), $prefix);
						}

						// get smallest thumb before 30x30
						$this->main_services[$sk]->thumb = $thumb_details['dirname'] . '/' . 'thumb_' . $this->default_thumb_sizes['_small'] .  '_' . $thumb_details['basename'];
					}
				}
			}
		}
		self::view(array('id' => $this->homepages[Registry()->locale]));
	}

	function view($params) {
		foreach ($params as $k => $v) {
			if (substr($k, 0, 3) == 'lvl' && !empty($v)) {
				$slug[] = $v;
			}

			if (is_null($v) || $v == '') {
				unset($params[$k]);
			}
		}


		if (!empty($params['id'])) {
			$this->obj = $this->Page->find_by_id((int) $params['id'], array('conditions' => 'visibility=2'));
		}

		$slug = join('/', $slug);

		// add / parsed slug is a string and does not have the slash "/" ahead
		(strlen($slug) > 1 && (!strstr($slug, '/') || stripos($slug, '/') > 0)) && ($slug = '/' . $slug);
		$params_slug = $slug;
		$this->params = $params;
		if (!$this->obj instanceOf Page) {
			$this->obj = $this->Page->find_by_slug($slug, array('conditions' => 'visibility=2'));
		}

		if (!$this->obj instanceOf Page) {
			$this->obj = $this->Page->find_by_old_slug($slug, array('conditions' => 'visibility=2'));
			if ($this->obj instanceOf Page) {
				Registry()->response->set_status(301);
				$this->redirect_to($this->obj->slug);
			}

			$this->obj = $this->Page->find_by_old_slug($slug.'?'.$_SERVER['QUERY_STRING'], array('conditions' => 'visibility=2'));
			if ($this->obj instanceOf Page) {
				Registry()->response->set_status(301);
				$this->redirect_to($this->obj->slug);
			}
		}
		if (!$this->obj instanceOf Page) {
			$module_slug = end($params);
			$slug = str_replace('/' . end($params), $this->slug_suffix, $slug);

			$slug && $this->obj = $this->Page->find_by_slug($slug, array('conditions' => 'visibility=2'));

			if (!$this->obj instanceOf Page) {
				$slug = substr($slug, 0, strrpos($slug, '/'));
				$slug && $this->obj = $this->Page->find_by_slug($slug, array('conditions' => 'visibility=2'));
				if (!$this->obj instanceOf Page) {
					$slug && $this->obj = $this->Page->find_by_old_slug($slug, array('conditions' => 'visibility=2'));
					if ($this->obj instanceOf Page) {
						Registry()->response->set_status(301);
						$this->redirect_to($this->obj->slug);
					}
				}

				if (!$this->obj instanceOf Page) {
					$slug = substr($slug, 0, strrpos($slug, '/'));
					$params_slug = $slug. str_replace($slug, '', $params_slug);
					$slug && $this->obj = $this->Page->find_by_slug($params_slug, array('conditions' => 'visibility=2'));
				}
			}
		}

		if (!$this->obj instanceOf Page) {
			//Registry()->response->set_status(301);
			//$this->redirect_to(url_for(array()));
			$this->error404();
		}

		$this->options = unserialize($this->obj->options);

		if ($this->obj->page_type == 'link') {
			if ($this->options['page_type'] == 'link') {
				$linkpage = $this->Page->find_by_id((int) $this->options['page_id']);
				//Registry()->response->set_status(301);
				$this->redirect_to(Config()->COOKIE_PATH . substr(Registry()->locale, 0, 2).'/'.trim($linkpage->slug,'/'));
			} else {
				$mirror = $this->Page->find_by_id((int) $this->options['page_id']);
				if ($mirror instanceOf Page) {
					$this->mirror_obj = $mirror;
				}
			}
		}

		if ($this->mirror_obj) {
			$this->placeholders = $this->mirror_obj->placeholders;
			$this->meta = array(
				'title' => $this->mirror_obj->meta_title,
				'description' => $this->mirror_obj->meta_description,
				'keywords' => $this->mirror_obj->meta_keywords
			);
			$this->canonical = url_for(array('action' => 'view', 'lvl1' => $this->mirror_obj->slug));
		} else {
			$this->placeholders = $this->obj->placeholders;
			$this->meta = array(
				'title' => $this->obj->meta_title,
				'description' => $this->obj->meta_description,
				'keywords' => $this->obj->meta_keywords
			);
		}

		if($this->obj->layout && $this->obj->layout != 'index') {
			$this->layout = $this->obj->layout;
		}

		if($this->obj) {
			$this->sub_menu = $this->obj->get_children(" AND visibility=2 AND exclude_from_menu = 0");
		}


		$this->options = unserialize($this->obj->options);
		if($this->options['carousels']) {
			/*$c = new CarouselsModel;
			$this->carousels = $c->find_all('id IN ('.join(',',$this->options['carousels']).')');

			$accent_model = new CarouselAccentsModel;
			$this->accent_text = $accent_model->find_first('accent_type=0 AND active=1');
			$this->accent_video = $accent_model->find_first('accent_type=1 AND active=1');*/
		}
		if(method_exists($this, 'bulcons_'.$this->obj->page_type)) {

			call_user_func(array($this, 'bulcons_'.$this->obj->page_type), $this->obj);
			$this->view = $this->obj->page_type;
			//gallery
		/*	$albums  = new AlbumsModel();
			$conditions = array(
				'conditions' => sprintf('page_id = %d AND active = 1', (int) $this->obj->id)
			);
			$this->album  = $albums->find_first($conditions, 'ord ASC');
			d($this->album);die;*/
		}


		$this->page_mirror = array();
		$langs = Registry()->db->select('pages_i18n','i18n_locale, slug',array('visibility'=>'2','i18n_foreign_key'=>$this->obj->id));
		foreach($langs as $lng) {
			$this->page_mirror[$lng->i18n_locale] = $lng->slug;
		}

		//$parents = $this->obj->get_parents();
		$this->breadcrumbs = array();
		foreach($parents as $p) {
			if($p->slug) {
				$this->breadcrumbs[] = array('title'=>$p->title, 'slug'=>$p->slug);
			}

		}

		$this->breadcrumbs = array_reverse($this->breadcrumbs);

	}



	function preview($params) {
		if (isset($this->session->userinfo)) {
			Registry()->is_preview = 1;
			self::view(array('id' => (int) $params['id']));
		}
	}

	// Call pages based on the page_type


	function bulcons_home($obj) {
		$carousels = new CarouselsModel();
		$this->slides = $carousels->find_all('active = 1', 'ord ASC');
		$accents = new AccentsModel();
		$this->accent_items = $accents->find_all('active = 1' , 'ord ASC');
		$recipes_model  = new RecipesModel();
		$this->recipe_on_focus = $recipes_model->find_first('active = 1 AND is_on_focus = 1');
		$news_model = new NewsModel();
		$this->news = $news_model->find_all('active = 1', ' published_datetime DESC ' , 3);
		$this->news_page_slug = $this->Page->find(20)->slug;
		$this->recipes_page_slug = $this->Page->find(16)->slug;
		$this->product_page_slug = $this->Page->find(9)->slug;
		$fake = new ProductsModel;
		$category = new ProductsCategory;
		$this->subcategories = $category->find_all(null,'ord ASC');
	}

	function bulcons_news_list($obj) {
		$this->news_page = $this->Page->find(20);
		$news_model = new NewsModel();
		$this->items = $news_model->find_all('active = 1', 'published_datetime DESC' , 6);
		//d($this->items);
		$this->paginator = new Paginator(current($this->items)->pages, $_GET['page']);
		
	}

	function bulcons_recipes($obj) {
		$fake = new RecipesModel;
		$category = new RecipesCategory;
		$this->subcategories = $category->find_all(null,'ord ASC');
		$this->recipes_list = $fake->find_all('active = 1','ord ASC',8);
		$this->paginator = new Paginator(current($this->recipes_list)->pages, $_GET['page']);
	}

	function bulcons_products($obj) {
		$product_fake = new ProductsModel;
		$product_category = new ProductsCategory;
		$this->products_list = $product_fake->find_all('active = 1','ord ASC',8);
		$this->paginator = new Paginator(current($this->products_list)->pages, $_GET['page']);
		$this->product_categories = $product_category->find_all(null,'ord ASC');
		$this->faqs_page = $this->Page->find(15);
	}

	function bulcons_faqs($obj) {
		$product_fake = new ProductsModel;
		$product_category = new ProductsCategory;
		$this->product_categories = $product_category->find_all(null,'ord ASC');
		$this->product_page = $this->Page->find(9);
		$faqs_model =  new FaqsModel;
		$this->faqs = $faqs_model->find_all('active = 1', 'ord ASC');
		//die('here');
	}

	function bulcons_about_us_subpages($obj) {
		//die('here');
		$certificates_model = new CertificationsModel;
		$this->certificates = $certificates_model->find_all('active = 1', 'ord ASC');
		$careers_model = new CareersModel;
		$this->careers = $careers_model->find_all('active = 1', 'ord ASC', 6);
		$this->paginator = new Paginator(current($this->careers)->pages, $_GET['page']);

		$response = new stdClass;
		$response->message = '';
		$response->errors = array();

		$career_params = array();
		if ($this->careers) {
			$career_params = array(
				'ref' => $this->careers->ref,
				'career_id' => $this->careers->id
			);
		}

		if($this->is_post()){
			//d($_POST);d($_FILES);die;
			$this->form_object = new CareersRequestsModel();
			if ($this->form_object->customSave($_POST + $career_params, $_FILES)) {
				$this->ok  = 1;
				$this->form_object->sendMailWithoutPosition();
			} else {
				//d($this->form_object->get_errors());
				$response->errors = $this->form_object->get_errors();
			}
		}
	}

	function bulcons_contact_us($obj) {
		$contact_model = new ContactsModel;
		$this->contact =  $contact_model->find_first('active = 1');
		$contact_departments_model = new ContactDepartmentsModel;
		$this->cd_objects = $contact_departments_model->find_all('active = 1');
		$this->categories = (new ContactDepartmentsCategory())->find_all();
		
		$this->suppliers_page_slug = $this->Page->find_by_id_and_visibility(23,2)->slug;
		$this->shops_page_slug = $this->Page->find(22)->slug;
		
		$response = new stdClass;
		$response->message = '';
		$response->errors = array();

		if($this->is_post()){
			$this->form_object = new ContactRequestModel();
			if($this->form_object->save($_POST)) {
				$this->ok = 1;
			} else {
				//d($this->form_object->get_errors());
				$response->errors = $this->form_object->errors;
			}
		}
	}

	function bulcons_suppliers($obj) {
		$this->contact_us_page_slug = $this->Page->find(21)->slug;
		$this->shops_page_slug = $this->Page->find(22)->slug;
		$user_model = new UsersModel;
		$this->users = $user_model->find_all();
		//d($this->users);
	}

	function bulcons_shops($obj) {
		$this->contact_us_page_slug = $this->Page->find(21)->slug;
		$this->suppliers_page_slug = $this->Page->find(23)->slug;
		$shops_model = new ShopsModel();
		$this->shops = $shops_model->find_all();

		$country_sql =  "SELECT DISTINCT (country) FROM shops_i18n WHERE country != '' ORDER BY country ASC";
		$countries = $shops_model->query($country_sql);

		foreach ($countries as $country) {
			$this->countries[] = $country->country;
		}
	}

	//result from filter 

	function load_shops($params){
		$shops_model = new ShopsModel();
		$conditions = array("lat > 0 AND lon > 0");

		if(!empty($params['country_id'])){
			//$conditions[] = sprintf("country LIKE '%%%s%%'", Registry()->db->escape($params['country_id']));
		}
		
		if(!empty($params['city_id'])){
			$conditions[] = sprintf("city LIKE '%%%s%%'", Registry()->db->escape($params['city_id']));
		}
		
		$sql_params = array();

		if(!empty($params['product_id'])){
		 	$sql_params['joins'] = sprintf("INNER JOIN products_shops ON products_shops.shop_id = shops.external_id AND products_shops.product_package_id = '%s'",Registry()->db->escape($params['product_id']));

	 	}

		
		if ($conditions) {
			$sql_params['conditions'] = '('.join(') AND (', $conditions).')';

		}

		$this->shops = $shops_model->find_all($sql_params);
		foreach($this->shops as $i) {
			$coordinates['items'][] = array(
				"lat" => $i->lat,
				"lon" => $i->lon,
				"title" => $i->name,				
				"address" => $i->street1 ? $i->street1 : $i->street2,
				"name" => $i->primary_contact ?: "",
				"contact" => $i->email ?: '',
				"worktime_label" => $this->localizer->get('DB_FIELDS', 'work_hours'),
				"worktime" => $i->working_time_from_monday_to_friday ?: '' . $i->working_time_saturday ?'</br> Събота:' . $i->working_time_saturday : '' . $i->working_time_sunday ? '</br> Неделя:' . $i->working_time_sunday : '',
			);
		}

		die(json_encode($coordinates));
	}

	protected function load_cities($params) {
		$return = array('success' => false);
		$shops_model = new ShopsModel();
		if($data = $shops_model->find_cities_for_public(!empty($params['selected']) ? $params['selected'] : null)) {
			$return['success'] = true;
			$return['attributes'] = $data;
		}

		$this->content_for_layout = json_encode($return);
	}

	protected function load_products($params) {
		$return = array('success' => false);
		$products_model = new ProductsModel();
		if($data = $products_model->find_product_packages_by_city_and_country(
			(!empty($params['selected']) ? $params['selected'] : null),
			(!empty($params['country_id']) ? $params['country_id'] : null)
		)) {
			$return['success'] = true;
			$return['attributes'] = $data;
		}

		$this->content_for_layout = json_encode($return);
	}

	public function change_state($params){
		$response = array();
		$shops_model = new ShopsModel();

		if(!empty($params['country'])) {
			$sql = "SELECT DISTINCT (city) FROM shops_i18n WHERE city != '' AND country = '" . $params['country'] . "' ORDER BY city ASC"; 
			$cities = $shops_model->query($sql);
			//find all packages with $params['country']
		}
		else {
			$sql = "SELECT DISTINCT (city) FROM shops_i18n WHERE city != '' ORDER BY city ASC";
			$cities = $shops_model->query($sql);		
		}

		if($cities){
				 //$response['options'][] = '<option value="">Град</option>';
			foreach ($cities as $item) {
				$response['options'][] = sprintf('<option value="%s">%s</option>', $item->city, $item->city);
			}

			$response['options'] = join('', $response['options']);
		}	

		 
		echo json_encode($response);
	}
}