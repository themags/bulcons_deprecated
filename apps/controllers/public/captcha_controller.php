<?php

class CaptchaController extends ApplicationController {
	protected $layout = 'xml_http_request';

	function index($params) {
		$image = $this->_generate(150, 40);
		header('Content-type: ' . $this->_getMimeTypeByFormat($image->getImageFormat()));
		$this->content_for_layout = $image;
		echo $image;
		exit;
	}

	private function _generate($width, $height) {
		$image = new Imagick();
		$image->newImage($width, $height, '#d6d0c9', 'png');

		$length = 4;
		$this->session->captcha = $text = $this->_randomText($length);

		$draw_text = new ImagickDraw();
		$draw_text->setFont(Config()->ROOT_PATH . 'web/fonts/' . 'rockwell.ttf');
		//$draw_text->setStrokeColor('#000');
		$draw_text->setFillColor('#f7f4f0');
		$draw_text->setFontSize(17);
		

		for($i = 0; $i < $length; ++$i) {
			$angle = 0;
			$top = 24;
			$left = ($i + 0.5) * 12 + 40;
			$image->annotateImage($draw_text, $left, $top, $angle, $text{$i});
		}



		return $image;
	}

	private function _getMimeTypeByFormat($format) {
		switch ($format) {
			case 'png':
				return 'image/png';
		}
	}

	private function _randomText($length) {
		$text = array();
		$range = array_merge(range('a','z'), range('A','Z'), range(0, 9));
		while(count($text) != $length)
		{
		   $text[] = $range[array_rand($range)];
		}
		return implode('', $text);
	}
}

?>