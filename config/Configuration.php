<?php

final class Config {
	/******************************************************************
	 * APPLICATION SETTINGS
	 ******************************************************************/

	/**
	 * Sets the application systems available
	 *
	 * Syntax is as follows: url_for_access => dirname
	 * Basicly once defined only the access urls can be changed.
	 * url_for_access with value of `default` is opened on the main domain
	 *
	 * @var array
	 * @access public
	 */
	public $APPLICATIONS = array(
		'default' 	=> 'public',
		'admin' 	=> 'admin',
		'modules'	=> 'modules',
	);

	public $VOTE_COOKIE_NAME = 'vote';

	public $MODULES = array(
		'admin',
	);

	/**
	 * The format of the supplied Cache driver:
	 * memcache://host:port:NS
	 * filesystem://
	 *
	 * @var string
	 * @access public
	 */
	//public $CACHE_DRIVER = 'memcache://127.0.0.1:11211:vivacom';
	public $CACHE_DRIVER = 'filesystem://127.0.0.1:11211:fw';

	/**
	 * Sets the enviroment mode
	 *
	 * @var boolean
	 * @access public
	 */
	public $DEVELOPMENT = true;


	/******************************************************************
	 * DATABASE SETTINGS
	 ******************************************************************/

	/**
	 * Data source name
	 *
	 * The format of the supplied DSN is in its fullest form:
	 *
	 *  driver://username:password@protocol+hostspec/database
	 *
	 * Most variations are allowed:
	 *
	 *  driver://username:password@protocol+hostspec:110//usr/db_file.db
	 *  driver://username:password@hostspec/database
	 *  driver://username:password@unix(/path/to/socket)/database
	 *  driver://username:password@hostspec
	 *  driver://username@hostspec
	 *  driver://hostspec/database
	 *  driver://hostspec
	 *  driver
	 *
	 * @var string
	 * @access public
	 */
	public $DSN = 'mysqli://themags:betamag@35.205.164.64/bulcons';

	/**
	 * Are database records mirrored for each language or not
	 *
	 * @var boolean
	 * @access public
	 */
	public $DB_MIRROR = false;

	/**
	 * Table prefix
	 *
	 * @var string
	 * @access public
	 */
	public $DB_PREFIX = '';

	/******************************************************************
	 * SESSION SETTINGS
	 ******************************************************************/

	/**
	 * Type of session
	 *
	 * - db - session store in database
	 * - standard - use session standard functions.
	 * - counter - use stantard php session with db active counter
	 * - redis - redis data structire server
	 *
	 * @var string
	 * @access public
	 */
	public $SESSION_TYPE = 'standard';


	/******************************************************************
	 * DEBUG SETTINGS
	 ******************************************************************/

	/**
	 * Mode for all exceptions. Combination of following: screen, mail, sms
	 *
	 * @var string
	 * @access public
	 */
	public $DEBUG_MODE = 'screen';

	/******************************************************************
	 * SMTP SETTINGS
	 ******************************************************************/

	 /**
	 * The format of the supplied SMTP is in its fullest form:
	 *
	 *  smtp://username:password@host:port/helo
	 *
	 * @var string
	 * @access public
	 */
	public $SMTP = 'smtp://web:xQh6ER8k0y@193.35.40.56:587';

	/******************************************************************
	 * INTERNATIONALIZATION SETTINGS
	 ******************************************************************/

	/**
	 * All possible languages for the application
	 * - keys -> shortcuts to the website
	 * - values -> according to IANA registry
	 *
	 * @var array
	 * @access public
	 */
	public $LOCALE_SHORTCUTS = array('bg' => 'bg-BG', 'en' => 'en-US', 'ru' => 'ru-RU');

	/**
	 * Default locale
	 * For structure look above description
	 *
	 * @var string
	 * @access public
	 */
	public $DEFAULT_LOCALE = 'bg-BG';


	/******************************************************************
	 * PATH SETTINGS
	 ******************************************************************/

	/**
	 * *nix file command path. Leave empty to use default include path.
	 *
	 * @var string
	 * @access public
	 */
	public $FILE_ROOT = "/usr/bin/";

	public $EMAILS_FROM = array('Tsvetomira Galcheva', 'tsvetomira.galcheva@themags.com');

	//public $DOMAIN = 'http://127.0.0.1/site name/'; //for subscriptions
}

?>
